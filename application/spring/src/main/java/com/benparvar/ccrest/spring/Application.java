package com.benparvar.ccrest.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The type Application.
 */
@Configuration
@EnableJpaRepositories("com.benparvar.ccrest.database.*")
@ComponentScan(basePackages = {"com.benparvar.ccrest.*"})
@EntityScan("com.benparvar.ccrest.*")
@EnableAutoConfiguration
@SpringBootApplication
public class Application {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
