package com.benparvar.ccrest.spring.config;

import com.benparvar.ccrest.controller.UserController;
import com.benparvar.ccrest.usecase.create.user.CreateUserUseCase;
import com.benparvar.ccrest.usecase.create.users.CreateUsersUseCase;
import com.benparvar.ccrest.usecase.delete.DeleteUserUseCase;
import com.benparvar.ccrest.usecase.read.user.cpf.FindUserByCpfUseCase;
import com.benparvar.ccrest.usecase.read.users.name.FindUsersByNameUseCase;
import com.benparvar.ccrest.usecase.read.users.surname.FindUsersBySurnameUseCase;
import com.benparvar.ccrest.usecase.update.UpdateUserUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Controller config.
 */
@Configuration
public class ControllerConfig {
    /**
     * User controller user controller.
     *
     * @param createUserUseCase         the create user use case
     * @param createUsersUseCase        the create users use case
     * @param deleteUserUseCase         the delete user use case
     * @param updateUserUseCase         the update user use case
     * @param findUserByCpfUseCase      the find user by cpf use case
     * @param findUsersByNameUseCase    the find users by name use case
     * @param findUsersBySurnameUseCase the find users by surname use case
     * @return the user controller
     */
    @Bean
    public UserController getUserController(CreateUserUseCase createUserUseCase,
                                            CreateUsersUseCase createUsersUseCase,
                                            DeleteUserUseCase deleteUserUseCase,
                                            UpdateUserUseCase updateUserUseCase,
                                            FindUserByCpfUseCase findUserByCpfUseCase,
                                            FindUsersByNameUseCase findUsersByNameUseCase,
                                            FindUsersBySurnameUseCase findUsersBySurnameUseCase) {
        return new UserController(createUserUseCase, createUsersUseCase, deleteUserUseCase, updateUserUseCase,
                findUserByCpfUseCase, findUsersByNameUseCase, findUsersBySurnameUseCase);
    }
}
