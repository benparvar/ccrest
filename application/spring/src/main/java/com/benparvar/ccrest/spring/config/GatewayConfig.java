package com.benparvar.ccrest.spring.config;

import com.benparvar.ccrest.database.h2db.UserDatabase;
import com.benparvar.ccrest.gateway.UserGateway;
import com.benparvar.ccrest.usecase.create.user.SaveUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Gateway config.
 */
@Configuration
public class GatewayConfig {

    /**
     * Gets save user.
     *
     * @param userDatabase the user database
     * @return the save user
     */
    @Bean
    public SaveUser getSaveUser(UserDatabase userDatabase) {
        return new UserGateway(userDatabase);
    }
}