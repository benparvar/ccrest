package com.benparvar.ccrest.spring.config;

import com.benparvar.ccrest.usecase.create.user.ActivateUserBeforeCreateUseCase;
import com.benparvar.ccrest.usecase.create.user.CreateUserUseCase;
import com.benparvar.ccrest.usecase.create.user.SaveUser;
import com.benparvar.ccrest.usecase.create.user.ValidateUserBeforeCreateUseCase;
import com.benparvar.ccrest.usecase.create.users.*;
import com.benparvar.ccrest.usecase.delete.DeleteUser;
import com.benparvar.ccrest.usecase.delete.DeleteUserUseCase;
import com.benparvar.ccrest.usecase.delete.ValidateCpfBeforeDeleteUserUseCase;
import com.benparvar.ccrest.usecase.read.user.cpf.FindUserByCpf;
import com.benparvar.ccrest.usecase.read.user.cpf.FindUserByCpfUseCase;
import com.benparvar.ccrest.usecase.read.user.cpf.ValidateCpfBeforeFindUserUseCase;
import com.benparvar.ccrest.usecase.read.users.name.FindUsersByName;
import com.benparvar.ccrest.usecase.read.users.name.FindUsersByNameUseCase;
import com.benparvar.ccrest.usecase.read.users.surname.FindUsersBySurname;
import com.benparvar.ccrest.usecase.read.users.surname.FindUsersBySurnameUseCase;
import com.benparvar.ccrest.usecase.update.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Use case config.
 */
@Configuration
public class UseCaseConfig {
    /**
     * Gets validate user use case.
     *
     * @return the validate user use case
     */
    @Bean
    public ValidateUserBeforeCreateUseCase getValidateUserUseCase() {
        return new ValidateUserBeforeCreateUseCase();
    }

    /**
     * Gets activate user use case.
     *
     * @return the activate user use case
     */
    @Bean
    public ActivateUserBeforeCreateUseCase getActivateUserUseCase() {
        return new ActivateUserBeforeCreateUseCase();
    }

    /**
     * Gets create user use case.
     *
     * @param validateUserBeforeCreateUseCase the validate user before create use case
     * @param activateUserBeforeCreateUseCase the activate user before create use case
     * @param saveUserGateway                 the save user gateway
     * @return the create user use case
     */
    @Bean
    public CreateUserUseCase getCreateUserUseCase(ValidateUserBeforeCreateUseCase validateUserBeforeCreateUseCase,
                                                  ActivateUserBeforeCreateUseCase activateUserBeforeCreateUseCase,
                                                  SaveUser saveUserGateway) {
        return new CreateUserUseCase(validateUserBeforeCreateUseCase, activateUserBeforeCreateUseCase, saveUserGateway);
    }

    /**
     * Gets validate users before create use case.
     *
     * @return the validate users before create use case
     */
    @Bean
    public ValidateUsersBeforeCreateUseCase getValidateUsersBeforeCreateUseCase() {
        return new ValidateUsersBeforeCreateUseCase();
    }

    /**
     * Gets find users by cpf list use case.
     *
     * @param findUsersByCpfListGateway the find users by cpf list gateway
     * @return the find users by cpf list use case
     */
    @Bean
    public FindUsersByCpfListUseCase getFindUsersByCpfListUseCase(FindUsersByCpfList findUsersByCpfListGateway) {
        return new FindUsersByCpfListUseCase(findUsersByCpfListGateway);
    }

    /**
     * Gets create users use case.
     *
     * @param validateUsersBeforeCreateUseCase  the validate users before create use case
     * @param separateActiveUsersUseCase        the separate active users use case
     * @param separateInactiveUsersUseCase      the separate inactive users use case
     * @param separateNonRegisteredUsersUseCase the separate non registered users use case
     * @param activateUsersBeforeCreateUseCase  the activate users before create use case
     * @param activateUsersAfterCreateUseCase   the activate users after create use case
     * @param findUsersByCpfListGateway         the find users by cpf list gateway
     * @param reactivateUsersGateway            the reactivate users gateway
     * @param saveUsersGateway                  the save users gateway
     * @return the create users use case
     */
    @Bean
    public CreateUsersUseCase getCreateUsersUseCase(ValidateUsersBeforeCreateUseCase validateUsersBeforeCreateUseCase,
                                                    SeparateActiveUsersUseCase separateActiveUsersUseCase,
                                                    SeparateInactiveUsersUseCase separateInactiveUsersUseCase,
                                                    SeparateNonRegisteredUsersUseCase separateNonRegisteredUsersUseCase,
                                                    ActivateUsersBeforeCreateUseCase activateUsersBeforeCreateUseCase,
                                                    ActivateUsersAfterCreateUseCase activateUsersAfterCreateUseCase,
                                                    FindUsersByCpfList findUsersByCpfListGateway,
                                                    ReactivateUsers reactivateUsersGateway,
                                                    SaveUsers saveUsersGateway) {
        return new CreateUsersUseCase(validateUsersBeforeCreateUseCase, separateActiveUsersUseCase, separateInactiveUsersUseCase,
                separateNonRegisteredUsersUseCase, activateUsersBeforeCreateUseCase, activateUsersAfterCreateUseCase,
                findUsersByCpfListGateway, reactivateUsersGateway, saveUsersGateway);
    }

    /**
     * Gets separate active users use case.
     *
     * @return the separate active users use case
     */
    @Bean
    public SeparateActiveUsersUseCase getSeparateActiveUsersUseCase() {
        return new SeparateActiveUsersUseCase();
    }

    /**
     * Gets separate inactive users use case.
     *
     * @return the separate inactive users use case
     */
    @Bean
    public SeparateInactiveUsersUseCase getSeparateInactiveUsersUseCase() {
        return new SeparateInactiveUsersUseCase();
    }

    /**
     * Gets separate non registered users use case.
     *
     * @return the separate non registered users use case
     */
    @Bean
    public SeparateNonRegisteredUsersUseCase getSeparateNonRegisteredUsersUseCase() {
        return new SeparateNonRegisteredUsersUseCase();
    }

    /**
     * Gets activate users before create use case.
     *
     * @return the activate users before create use case
     */
    @Bean
    public ActivateUsersBeforeCreateUseCase getActivateUsersBeforeCreateUseCase() {
        return new ActivateUsersBeforeCreateUseCase();
    }

    /**
     * Gets activate users after create use case.
     *
     * @return the activate users after create use case
     */
    @Bean
    public ActivateUsersAfterCreateUseCase getActivateUsersAfterCreateUseCase() {
        return new ActivateUsersAfterCreateUseCase();
    }

    /**
     * Gets validate cpf before delete user use case.
     *
     * @return the validate cpf before delete user use case
     */
    @Bean
    public ValidateCpfBeforeDeleteUserUseCase getValidateCpfBeforeDeleteUserUseCase() {
        return new ValidateCpfBeforeDeleteUserUseCase();
    }

    /**
     * Gets delete user use case.
     *
     * @param validateCpfBeforeDeleteUserUseCase the validate cpf before delete user use case
     * @param deleteUserGateway                  the delete user gateway
     * @return the delete user use case
     */
    @Bean
    public DeleteUserUseCase getDeleteUserUseCase(ValidateCpfBeforeDeleteUserUseCase validateCpfBeforeDeleteUserUseCase,
                                                  DeleteUser deleteUserGateway) {
        return new DeleteUserUseCase(validateCpfBeforeDeleteUserUseCase, deleteUserGateway);
    }

    /**
     * Get validate user to update before transform to user validate user to update before transform to user.
     *
     * @return the validate user to update before transform to user
     */
    @Bean
    public ValidateUserToUpdateBeforeTransformToUserUseCase getValidateUserToUpdateBeforeTransformToUser() {
        return new ValidateUserToUpdateBeforeTransformToUserUseCase();
    }

    /**
     * Validate user before update use case validate user before update use case.
     *
     * @return the validate user before update use case
     */
    @Bean
    public ValidateUserBeforeUpdateUseCase ValidateUserBeforeUpdateUseCase() {
        return new ValidateUserBeforeUpdateUseCase();
    }

    /**
     * Gets validate cpf before update user use case.
     *
     * @return the validate cpf before update user use case
     */
    @Bean
    public ValidateCpfBeforeUpdateUserUseCase getValidateCpfBeforeUpdateUserUseCase() {
        return new ValidateCpfBeforeUpdateUserUseCase();
    }

    /**
     * Gets update user use case.
     *
     * @param validateCpfBeforeUpdateUserUseCase               the validate cpf before update user use case
     * @param validateUserBeforeUpdateUseCase                  the validate user before update use case
     * @param validateUserToUpdateBeforeTransformToUserUseCase the validate user to update before transform to user
     * @param updateUserGateway                                the update user gateway
     * @return the update user use case
     */
    @Bean
    public UpdateUserUseCase getUpdateUserUseCase(ValidateCpfBeforeUpdateUserUseCase validateCpfBeforeUpdateUserUseCase,
                                                  ValidateUserBeforeUpdateUseCase validateUserBeforeUpdateUseCase,
                                                  ValidateUserToUpdateBeforeTransformToUserUseCase validateUserToUpdateBeforeTransformToUserUseCase,
                                                  UpdateUser updateUserGateway) {
        return new UpdateUserUseCase(validateCpfBeforeUpdateUserUseCase, validateUserBeforeUpdateUseCase,
                validateUserToUpdateBeforeTransformToUserUseCase, updateUserGateway);
    }

    /**
     * Gets validate cpf before find user use case.
     *
     * @return the validate cpf before find user use case
     */
    @Bean
    public ValidateCpfBeforeFindUserUseCase getValidateCpfBeforeFindUserUseCase() {
        return new ValidateCpfBeforeFindUserUseCase();
    }

    /**
     * Gets find user by cpf use case.
     *
     * @param validateCpfBeforeFindUserUseCase the validate cpf before find user use case
     * @param findUserByCpfGateway             the find user by cpf gateway
     * @return the find user by cpf use case
     */
    @Bean
    public FindUserByCpfUseCase getFindUserByCpfUseCase(ValidateCpfBeforeFindUserUseCase validateCpfBeforeFindUserUseCase,
                                                        FindUserByCpf findUserByCpfGateway) {
        return new FindUserByCpfUseCase(validateCpfBeforeFindUserUseCase, findUserByCpfGateway);
    }

    /**
     * Gets find users by name use case.
     *
     * @param findUsersByNameGateway the find users by name gateway
     * @return the find users by name use case
     */
    @Bean
    public FindUsersByNameUseCase getFindUsersByNameUseCase(FindUsersByName findUsersByNameGateway) {
        return new FindUsersByNameUseCase(findUsersByNameGateway);
    }

    /**
     * Gets find users by surname use case.
     *
     * @param findUsersBySurnameGateway the find users by surname gateway
     * @return the find users by surname use case
     */
    @Bean
    public FindUsersBySurnameUseCase getFindUsersBySurnameUseCase(FindUsersBySurname findUsersBySurnameGateway) {
        return new FindUsersBySurnameUseCase(findUsersBySurnameGateway);
    }
}
