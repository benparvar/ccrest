package com.benparvar.ccrest.spring.controller;

import com.benparvar.ccrest.controller.UserController;
import com.benparvar.ccrest.controller.model.create.user.CreateUserRequest;
import com.benparvar.ccrest.controller.model.create.user.CreateUserResponse;
import com.benparvar.ccrest.controller.model.read.user.ReadUserResponse;
import com.benparvar.ccrest.controller.model.update.user.UpdateUserRequest;
import com.benparvar.ccrest.usecase.exception.UserAlreadyExistsException;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

/**
 * The type Spring user controller.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class SpringUserController {
    private final Logger log = LoggerFactory.getLogger(SpringUserController.class);
    @Autowired
    private final UserController controller;

    /**
     * Instantiates a new Spring user controller.
     *
     * @param controller the controller
     */
    public SpringUserController(UserController controller) {
        this.controller = controller;
    }

    /**
     * Create create user response.
     *
     * @param request the request
     * @return the create user response
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CreateUserResponse create(@RequestBody CreateUserRequest request) {
        log.info("create: {}", request);

        return controller.create(request);
    }

    /**
     * Bulk create list.
     *
     * @param request the request
     * @return the list
     */
    @PostMapping(value = "/bulk", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public List<CreateUserResponse> bulkCreate(@RequestBody List<CreateUserRequest> request) {
        log.info("bulkCreate: {}", request);

        return controller.bulkCreate(request);
    }

    /**
     * Delete.
     *
     * @param cpf the cpf
     */
    @DeleteMapping(value = "/{cpf}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String cpf) {
        log.info("delete: {}", cpf);
        controller.delete(cpf);
    }

    /**
     * Update.
     *
     * @param request the request
     * @param cpf     the cpf
     */
    @PutMapping(value = "/{cpf}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody UpdateUserRequest request, @PathVariable String cpf) {
        log.info("update: cpf {} - request {}", cpf, request);
        controller.update(request, cpf);
    }

    /**
     * Find by cpf read user response.
     *
     * @param cpf the cpf
     * @return the read user response
     */
    @GetMapping(value = "/{cpf}/cpf")
    @ResponseStatus(HttpStatus.OK)
    public ReadUserResponse findByCpf(@PathVariable String cpf) {
        log.info("findByCpf: {}", cpf);

        return controller.findByCpf(cpf);
    }

    /**
     * Find by name list.
     *
     * @param name the name
     * @return the list
     */
    @GetMapping(value = "/{name}/name")
    @ResponseStatus(HttpStatus.OK)
    public List<ReadUserResponse> findByName(@PathVariable String name) {
        log.info("findByName: {}", name);

        return controller.findByName(name);
    }

    /**
     * Find by surname list.
     *
     * @param surname the surname
     * @return the list
     */
    @GetMapping(value = "/{surname}/surname")
    @ResponseStatus(HttpStatus.OK)
    public List<ReadUserResponse> findBySurname(@PathVariable String surname) {
        log.info("findBySurname: {}", surname);

        return controller.findBySurname(surname);
    }

    /**
     * Handle illegal argument exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle user not found exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Sorry, not found.", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    /**
     * Handle user already exists exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({UserAlreadyExistsException.class})
    public ResponseEntity<Object> handleUserAlreadyExistsException(UserAlreadyExistsException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Sorry, already exist.", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleException(Exception ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Sorry, we were unable to process your request.", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
