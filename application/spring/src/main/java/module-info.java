module com.benparvar.ccrest.spring {
    requires com.benparvar.ccrest.usecase;
    requires com.benparvar.ccrest.controller;
    requires com.benparvar.ccrest.gateway;
    requires com.benparvar.ccrest.database;
    requires spring.web;
    requires spring.beans;

    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires com.fasterxml.jackson.databind;
    requires jackson.annotations;
    requires org.slf4j;
    requires spring.data.jpa;

    exports com.benparvar.ccrest.spring;
    exports com.benparvar.ccrest.spring.config;
    opens com.benparvar.ccrest.spring.config to spring.core;
}