package com.benparvar.ccrest.spring.controller;

import com.benparvar.ccrest.spring.controller.fixture.UserFixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Spring user controller test.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class SpringUserControllerTest {
    private final String CPF = "276.193.418-03";
    private final String NAME = "Alan";
    private final String SURNAME = "Alencar da Silva";

    @Autowired
    private MockMvc mvc;

    /**
     * Insert with empty payload.
     *
     * @throws Exception the exception
     */
    @Test
    public void insertWithEmptyPayload() throws Exception {
        mvc.perform(post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_EMPTY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Insert with invalid payload.
     *
     * @throws Exception the exception
     */
    @Test
    public void insertWithInvalidPayload() throws Exception {
        mvc.perform(post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_INVALID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Insert with valid payload.
     *
     * @throws Exception the exception
     */
    @Test
    public void insertWithValidPayload() throws Exception {
        mvc.perform(post("/api/v1/users")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    /**
     * Insert bulk with empty payload.
     *
     * @throws Exception the exception
     */
    @Test
    public void insertBulkWithEmptyPayload() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_EMPTY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Insert bulk with valid payload.
     *
     * @throws Exception the exception
     */
    @Test
    public void insertBulkWithValidPayload() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    /**
     * Update non existent user.
     *
     * @throws Exception the exception
     */
    @Test
    public void updateNonExistentUser() throws Exception {
        mvc.perform(put("/api/v1/users/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Update existent user.
     *
     * @throws Exception the exception
     */
    @Test
    public void updateExistentUser() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(put("/api/v1/users/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_UPDATE_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    /**
     * Delete non existent user.
     *
     * @throws Exception the exception
     */
    @Test
    public void deleteNonExistentUser() throws Exception {
        mvc.perform(delete("/api/v1/users/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    /**
     * Delete existent user.
     *
     * @throws Exception the exception
     */
    @Test
    public void deleteExistentUser() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(delete("/api/v1/users/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    /**
     * Find by name.
     *
     * @throws Exception the exception
     */
    @Test
    public void findByName() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(get("/api/v1/users/" + NAME + "/name")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UserFixture.RESPONSE_BULK_OK))
                .andReturn();
    }

    /**
     * Find by surname.
     *
     * @throws Exception the exception
     */
    @Test
    public void findBySurname() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(get("/api/v1/users/" + SURNAME + "/surname")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UserFixture.RESPONSE_BULK_OK))
                .andReturn();
    }

    /**
     * Find by cpf.
     *
     * @throws Exception the exception
     */
    @Test
    public void findByCpf() throws Exception {
        mvc.perform(post("/api/v1/users/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UserFixture.REQUEST_BULK_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(get("/api/v1/users/" + CPF + "/cpf")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UserFixture.RESPONSE_OK))
                .andReturn();
    }

}