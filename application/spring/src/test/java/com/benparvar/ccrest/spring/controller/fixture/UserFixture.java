package com.benparvar.ccrest.spring.controller.fixture;

/**
 * The type User fixture.
 */
public class UserFixture {
    /**
     * The constant REQUEST_EMPTY.
     */
    public static String REQUEST_EMPTY = "{}";

    /**
     * The constant REQUEST_BULK_EMPTY.
     */
    public static String REQUEST_BULK_EMPTY = "[]";

    /**
     * The constant REQUEST_INVALID.
     */
    public static String REQUEST_INVALID = "{\n" +
            "\t\"cdf\": \"276.193.418-03\",\n" +
            "\t\"name\": \"Alan\",\n" +
            "\t\"phones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"birthDate\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"address\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "}";

    /**
     * The constant REQUEST_OK.
     */
    public static String REQUEST_OK = "{\n" +
            "\t\"cpf\": \"276.193.418-03\",\n" +
            "\t\"name\": \"Alan\",\n" +
            "\t\"surname\": \"Alencar da Silva\",\n" +
            "\t\"phones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"birthDate\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"address\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "}";

    public static String REQUEST_UPDATE_OK = "{\n" +
            "\t\"cpf\": \"276.193.418-03\",\n" +
            "\t\"name\": \"Alan\",\n" +
            "\t\"surname\": \"Alencar da Silva\",\n" +
            "\t\"phones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"birthDate\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"address\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\",\n" +
            "\t\"active\": true\n" +
            "}";

    /**
     * The constant REQUEST_BULK_OK.
     */
    public static String REQUEST_BULK_OK = "[{\t\n" +
            "\t\"cpf\": \"276.193.418-03\",\n" +
            "\t\"name\": \"Alan\",\n" +
            "\t\"surname\": \"Alencar da Silva\",\n" +
            "\t\"phones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"birthDate\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"address\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "\t\n" +
            "},\n" +
            "{\t\n" +
            "\t\"cpf\": \"274.553.606-06\",\n" +
            "\t\"name\": \"Alone\",\n" +
            "\t\"surname\": \"da Silva\",\n" +
            "\t\"phones\" : [\"(11) 94379-3112\", \"(11) 4816-2453\"],\n" +
            "\t\"birthDate\": \"1990-03-28\",\n" +
            "\t\"emails\": [\"alone@gmail.com\"],\n" +
            "\t\"address\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "\t\n" +
            "}\n" +
            "]";

    /**
     * The constant RESPONSE_OK.
     */
    public static String RESPONSE_OK = "{\n" +
            "    \"cpf\": \"276.193.418-03\",\n" +
            "    \"name\": \"Alan\",\n" +
            "    \"surname\": \"Alencar da Silva\",\n" +
            "    \"birthDate\": \"1980-01-10\",\n" +
            "    \"address\": \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\",\n" +
            "    \"phones\": [\n" +
            "        \"(11) 94379-3198\",\n" +
            "        \"(11) 4816-2453\"\n" +
            "    ],\n" +
            "    \"emails\": [\n" +
            "        \"benparvar@gmail.com\"\n" +
            "    ],\n" +
            "    \"active\": true\n" +
            "}";

    /**
     * The constant RESPONSE_BULK_OK.
     */
    public static String RESPONSE_BULK_OK = "[{\n" +
            "    \"cpf\": \"276.193.418-03\",\n" +
            "    \"name\": \"Alan\",\n" +
            "    \"surname\": \"Alencar da Silva\",\n" +
            "    \"birthDate\": \"1980-01-10\",\n" +
            "    \"address\": \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\",\n" +
            "    \"phones\": [\n" +
            "        \"(11) 94379-3198\",\n" +
            "        \"(11) 4816-2453\"\n" +
            "    ],\n" +
            "    \"emails\": [\n" +
            "        \"benparvar@gmail.com\"\n" +
            "    ],\n" +
            "    \"active\": true\n" +
            "}]";
}
