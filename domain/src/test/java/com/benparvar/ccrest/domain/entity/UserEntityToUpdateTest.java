package com.benparvar.ccrest.domain.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type User entity to update test.
 */
class UserEntityToUpdateTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UserEntityToUpdate obj1 = UserEntityToUpdate.newBuilder()
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        UserEntityToUpdate obj2 = UserEntityToUpdate.newBuilder()
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        assertEquals(obj1.hashCode(), obj2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UserEntityToUpdate obj1 = UserEntityToUpdate.newBuilder()
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        UserEntityToUpdate obj2 = UserEntityToUpdate.newBuilder()
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        assertEquals(obj1, obj2);
    }

    @Test
    public void checkBuilderIntegrity() {
        UserEntityToUpdate obj = UserEntityToUpdate.newBuilder()
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        assertEquals("Hashy", obj.getName());
        assertEquals("Ilusion", obj.getSurname());
        assertEquals(LocalDate.of(2019, 9, 8), obj.getBirthDate());
        assertEquals("Desulional St, 21", obj.getAddress());
        assertEquals(Arrays.asList("del@ded.gov"), obj.getEmails());
        assertEquals(Arrays.asList("(11) 8888-1231"), obj.getPhones());
        assertEquals(Boolean.TRUE, obj.getActive());
    }
}