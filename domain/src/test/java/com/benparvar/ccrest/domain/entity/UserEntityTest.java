package com.benparvar.ccrest.domain.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type User entity test.
 */
class UserEntityTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UserEntity obj1 = UserEntity.newBuilder()
                .id(1L)
                .cpf("336.519.850-40")
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.FALSE)
                .build();

        UserEntity obj2 = UserEntity.newBuilder()
                .id(1L)
                .cpf("336.519.850-40")
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.FALSE)
                .build();

        assertEquals(obj1.hashCode(), obj2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UserEntity obj1 = UserEntity.newBuilder()
                .id(1L)
                .cpf("336.519.850-40")
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        UserEntity obj2 = UserEntity.newBuilder()
                .id(1L)
                .cpf("336.519.850-40")
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        assertEquals(obj1, obj2);
    }

    @Test
    public void checkBuilderIntegrity() {
        UserEntity obj = UserEntity.newBuilder()
                .id(1L)
                .name("Hashy")
                .surname("Ilusion")
                .birthDate(LocalDate.of(2019, 9, 8))
                .address("Desulional St, 21")
                .emails(Arrays.asList("del@ded.gov"))
                .phones(Arrays.asList("(11) 8888-1231"))
                .active(Boolean.TRUE)
                .build();

        assertEquals(1L, obj.getId());
        assertEquals("Hashy", obj.getName());
        assertEquals("Ilusion", obj.getSurname());
        assertEquals(LocalDate.of(2019, 9, 8), obj.getBirthDate());
        assertEquals("Desulional St, 21", obj.getAddress());
        assertEquals(Arrays.asList("del@ded.gov"), obj.getEmails());
        assertEquals(Arrays.asList("(11) 8888-1231"), obj.getPhones());
        assertEquals(Boolean.TRUE, obj.getActive());
    }
}