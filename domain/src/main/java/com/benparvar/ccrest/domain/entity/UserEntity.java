package com.benparvar.ccrest.domain.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * The type User entity.
 */
public class UserEntity implements Serializable {
    private static final long serialVersionUID = -1405201991529574128L;
    private Long id;
    private String cpf;
    private String name;
    private String surname;
    private LocalDate birthDate;
    private String address;
    private List<String> phones;
    private List<String> emails;
    private Boolean active;

    private UserEntity(Builder builder) {
        setId(builder.id);
        setCpf(builder.cpf);
        setName(builder.name);
        setSurname(builder.surname);
        setBirthDate(builder.birthDate);
        setAddress(builder.address);
        setPhones(builder.phones);
        setEmails(builder.emails);
        setActive(builder.active);
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private Long id;
        private String cpf;
        private String name;
        private String surname;
        private LocalDate birthDate;
        private String address;
        private List<String> phones;
        private List<String> emails;
        private Boolean active;

        /**
         * Instantiates a new Builder.
         */
        Builder() {
        }

        /**
         * Id builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder id(Long val) {
            id = val;
            return this;
        }

        /**
         * Cpf builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder cpf(String val) {
            cpf = val;
            return this;
        }

        /**
         * Name builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder name(String val) {
            name = val;
            return this;
        }

        /**
         * Surname builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder surname(String val) {
            surname = val;
            return this;
        }

        /**
         * Birth date builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder birthDate(LocalDate val) {
            birthDate = val;
            return this;
        }

        /**
         * Address builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder address(String val) {
            address = val;
            return this;
        }

        /**
         * Phones builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder phones(List<String> val) {
            phones = val;
            return this;
        }

        /**
         * Emails builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder emails(List<String> val) {
            emails = val;
            return this;
        }

        /**
         * Active builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder active(Boolean val) {
            active = val;
            return this;
        }

        /**
         * Build user entity.
         *
         * @return the user entity
         */
        public UserEntity build() {
            return new UserEntity(this);
        }
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets cpf.
     *
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets phones.
     *
     * @return the phones
     */
    public List<String> getPhones() {
        return phones;
    }

    /**
     * Gets emails.
     *
     * @return the emails
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    private void setId(Long id) {
        this.id = id;
    }

    private void setCpf(String cpf) {
        this.cpf = cpf;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setSurname(String surname) {
        this.surname = surname;
    }

    private void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    private void setPhones(List<String> phones) {
        this.phones = phones;
    }

    private void setEmails(List<String> emails) {
        this.emails = emails;
    }

    private void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity userEntity = (UserEntity) o;
        return Objects.equals(cpf, userEntity.cpf) &&
                Objects.equals(name, userEntity.name) &&
                Objects.equals(surname, userEntity.surname) &&
                Objects.equals(birthDate, userEntity.birthDate) &&
                Objects.equals(address, userEntity.address) &&
                Objects.equals(phones, userEntity.phones) &&
                Objects.equals(emails, userEntity.emails) &&
                Objects.equals(active, userEntity.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cpf, name, surname, birthDate, address, phones, emails, active);
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", cpf='" + cpf + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", phones=" + phones +
                ", emails=" + emails +
                ", active=" + active +
                '}';
    }
}
