package com.benparvar.ccrest.domain.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * The type User entity to update.
 */
public class UserEntityToUpdate implements Serializable {
    private static final long serialVersionUID = -8082862343899580821L;
    private final String name;
    private final String surname;
    private final LocalDate birthDate;
    private final String address;
    private final List<String> phones;
    private final List<String> emails;
    private final Boolean active;

    private UserEntityToUpdate(Builder builder) {
        name = builder.name;
        surname = builder.surname;
        birthDate = builder.birthDate;
        address = builder.address;
        phones = builder.phones;
        emails = builder.emails;
        active = builder.active;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets phones.
     *
     * @return the phones
     */
    public List<String> getPhones() {
        return phones;
    }

    /**
     * Gets emails.
     *
     * @return the emails
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String name;
        private String surname;
        private LocalDate birthDate;
        private String address;
        private List<String> phones;
        private List<String> emails;
        private Boolean active;

        private Builder() {
        }

        /**
         * Name builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder name(String val) {
            name = val;
            return this;
        }

        /**
         * Surname builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder surname(String val) {
            surname = val;
            return this;
        }

        /**
         * Birth date builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder birthDate(LocalDate val) {
            birthDate = val;
            return this;
        }

        /**
         * Address builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder address(String val) {
            address = val;
            return this;
        }

        /**
         * Phones builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder phones(List<String> val) {
            phones = val;
            return this;
        }

        /**
         * Emails builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder emails(List<String> val) {
            emails = val;
            return this;
        }

        /**
         * Active builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder active(Boolean val) {
            active = val;
            return this;
        }

        /**
         * Build user entity to update.
         *
         * @return the user entity to update
         */
        public UserEntityToUpdate build() {
            return new UserEntityToUpdate(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntityToUpdate that = (UserEntityToUpdate) o;
        return name.equals(that.name) &&
                surname.equals(that.surname) &&
                birthDate.equals(that.birthDate) &&
                address.equals(that.address) &&
                phones.equals(that.phones) &&
                emails.equals(that.emails) &&
                active.equals(that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, address, phones, emails, active);
    }

    @Override
    public String toString() {
        return "UserEntityToUpdate{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", phones=" + phones +
                ", emails=" + emails +
                ", active=" + active +
                '}';
    }
}
