# ccrest

A very simple (Clean Architecture) CRUD.

## Requirements
* Java SE 11
* Gradle 5

## Frameworks
* Springboot
* Swagger
* Mapstruct
* Mockito
* Junit 5
* Jacoco

## Database
* H2  "in memorian" database

## Building Project
    gradlew clean build
    
## Running Spring Project
    `java -jar application/spring-app/build/libs/spring-app-1.0.0.jar`

## Running on IDE
gradle bootRun

## Unit Test
    gradlew test

## Javadoc
    gradlew javadoc

## Docker
* Generating the image
    gradlew clean build buildDocker
* Running the image
    docker run -p 9876:9876 benparvar/ccrest:1.0.0

## Run Application
    gradlew -Penv="[ENV]" bootRun

ENV: "dev", "qa", "staging" or "prod"

Default is "dev" (if "env" parameter is not defined)

## REST Documentation
Please see the link http://{server}:9876/swagger-ui.html



