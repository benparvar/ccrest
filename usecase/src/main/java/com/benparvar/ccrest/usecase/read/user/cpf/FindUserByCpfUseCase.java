package com.benparvar.ccrest.usecase.read.user.cpf;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Find user by cpf use case.
 */
public class FindUserByCpfUseCase {
    private final Logger log = LoggerFactory.getLogger(FindUserByCpfUseCase.class);

    private final FindUserByCpf findUserByCpfGateway;
    private final ValidateCpfBeforeFindUserUseCase validateCpfBeforeFindUserUseCase;

    /**
     * Instantiates a new Find user by cpf use case.
     *
     * @param validateCpfBeforeFindUserUseCase the validate cpf before find user use case
     * @param findUserByCpfGateway             the find user by cpf gateway
     */
    public FindUserByCpfUseCase(ValidateCpfBeforeFindUserUseCase validateCpfBeforeFindUserUseCase,
                                FindUserByCpf findUserByCpfGateway) {
        this.findUserByCpfGateway = findUserByCpfGateway;
        this.validateCpfBeforeFindUserUseCase = validateCpfBeforeFindUserUseCase;
    }

    /**
     * Execute user entity.
     *
     * @param cpf the cpf
     * @return the user entity
     */
    public UserEntity execute(String cpf) {
        log.info("execute: {}", cpf);
        validateCpfBeforeFindUserUseCase.execute(cpf);
        UserEntity entity = findUserByCpfGateway.findByCpf(cpf);
        if (null == entity)
            throw new UserNotFoundException();

        return entity;
    }
}
