package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Create users use case.
 */
public class CreateUsersUseCase {
    private final Logger log = LoggerFactory.getLogger(CreateUsersUseCase.class);

    private final FindUsersByCpfList findUsersByCpfListGateway;
    private final SaveUsers saveUsersGateway;
    private final ReactivateUsers reactivateUsersGateway;
    private final ValidateUsersBeforeCreateUseCase validateUsersBeforeCreateUseCase;
    private final SeparateActiveUsersUseCase separateActiveUsersUseCase;
    private final SeparateInactiveUsersUseCase separateInactiveUsersUseCase;
    private final SeparateNonRegisteredUsersUseCase separateNonRegisteredUsersUseCase;
    private final ActivateUsersBeforeCreateUseCase activateUsersBeforeCreateUseCase;
    private final ActivateUsersAfterCreateUseCase activateUsersAfterCreateUseCase;

    /**
     * Instantiates a new Create users use case.
     *
     * @param validateUsersBeforeCreateUseCase  the validate users before create use case
     * @param separateActiveUsersUseCase        the separate active users use case
     * @param separateInactiveUsersUseCase      the separate inactive users use case
     * @param separateNonRegisteredUsersUseCase the separate non registered users use case
     * @param activateUsersBeforeCreateUseCase  the activate users before create use case
     * @param activateUsersAfterCreateUseCase   the activate users after create use case
     * @param findUsersByCpfListGateway         the find users by cpf list gateway
     * @param reactivateUsersGateway            the reactivate users gateway
     * @param saveUsersGateway                  the save users gateway
     */
    public CreateUsersUseCase(ValidateUsersBeforeCreateUseCase validateUsersBeforeCreateUseCase,
                              SeparateActiveUsersUseCase separateActiveUsersUseCase,
                              SeparateInactiveUsersUseCase separateInactiveUsersUseCase,
                              SeparateNonRegisteredUsersUseCase separateNonRegisteredUsersUseCase,
                              ActivateUsersBeforeCreateUseCase activateUsersBeforeCreateUseCase,
                              ActivateUsersAfterCreateUseCase activateUsersAfterCreateUseCase,
                              FindUsersByCpfList findUsersByCpfListGateway,
                              ReactivateUsers reactivateUsersGateway,
                              SaveUsers saveUsersGateway) {
        this.findUsersByCpfListGateway = findUsersByCpfListGateway;
        this.saveUsersGateway = saveUsersGateway;
        this.reactivateUsersGateway = reactivateUsersGateway;
        this.validateUsersBeforeCreateUseCase = validateUsersBeforeCreateUseCase;
        this.separateActiveUsersUseCase = separateActiveUsersUseCase;
        this.separateInactiveUsersUseCase = separateInactiveUsersUseCase;
        this.separateNonRegisteredUsersUseCase = separateNonRegisteredUsersUseCase;
        this.activateUsersBeforeCreateUseCase = activateUsersBeforeCreateUseCase;
        this.activateUsersAfterCreateUseCase = activateUsersAfterCreateUseCase;
    }

    /**
     * Execute list.
     *
     * @param userEntities the user entities
     * @return the list
     */
    public List<UserEntity> execute(List<UserEntity> userEntities) {
        log.info("execute: {}", userEntities);
        validateUsersBeforeCreateUseCase.execute(userEntities);

        List<String> cpfList = userEntities.stream()
                .map(UserEntity::getCpf)
                .collect(Collectors.toList());
        List<UserEntity> allRegisteredUsers = findUsersByCpfListGateway.findByCpfIn(cpfList);

        // Active and registered users
        List<UserEntity> untouchedAlreadyRegisteredUsers = separateActiveUsersUseCase.execute(allRegisteredUsers);

        // Inactive and registered users
        int inactiveRegisteredUsers = reactivateUsersGateway.reactivate(separateInactiveUsersUseCase.execute(allRegisteredUsers));

        // Non registered users
        List<UserEntity> newRegisteredUsers = saveUsersGateway.saveUser(activateUsersBeforeCreateUseCase
                .execute(separateNonRegisteredUsersUseCase.execute(userEntities, allRegisteredUsers)));

        log.info("Untouched  users -> Already registered and active users: {}", untouchedAlreadyRegisteredUsers.size());
        log.info("Reactivated users -> Already registered and inactive users: {}", inactiveRegisteredUsers);
        log.info("New users -> Registered and activated users: {}", newRegisteredUsers.size());

        return activateUsersAfterCreateUseCase.execute(userEntities);
    }
}
