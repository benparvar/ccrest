package com.benparvar.ccrest.usecase.validator;

import java.util.Collection;

/**
 * The type Collection utils.
 */
public interface CollectionUtils {
    /**
     * Is empty boolean.
     *
     * @param collection the collection
     * @return the boolean
     */
    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }
}
