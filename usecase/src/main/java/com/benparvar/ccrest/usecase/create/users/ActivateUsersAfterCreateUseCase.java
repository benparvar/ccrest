package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Activate users after create use case.
 */
public class ActivateUsersAfterCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ActivateUsersAfterCreateUseCase.class);

    private static final String USER_ENTITY_LIST_CANNOT_BE_NULL = "User entity list cannot be null";
    private static final String USER_CANNOT_BE_NULL = "User entity cannot be null";

    /**
     * Execute list.
     *
     * @param userEntities the user entities
     * @return the list
     */
    public List<UserEntity> execute(List<UserEntity> userEntities) {
        log.info("execute: {}", userEntities);
        Assert.notNull(userEntities, USER_ENTITY_LIST_CANNOT_BE_NULL);

        return userEntities.stream()
                .map(this::activate)
                .collect(Collectors.toList());
    }

    private UserEntity activate(UserEntity userEntity) {
        log.info("activate: {}", userEntity);
        Assert.notNull(userEntity, USER_CANNOT_BE_NULL);
        return UserEntity.newBuilder()
                .id(userEntity.getId())
                .cpf(userEntity.getCpf())
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .birthDate(userEntity.getBirthDate())
                .address(userEntity.getAddress())
                .phones(userEntity.getPhones())
                .emails(userEntity.getEmails())
                .active(Boolean.TRUE)
                .build();
    }
}
