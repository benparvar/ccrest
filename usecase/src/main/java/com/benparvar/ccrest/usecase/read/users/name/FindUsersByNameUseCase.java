package com.benparvar.ccrest.usecase.read.users.name;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Find users by name use case.
 */
public class FindUsersByNameUseCase {
    private final Logger log = LoggerFactory.getLogger(FindUsersByNameUseCase.class);

    private final FindUsersByName findUsersByNameGateway;

    /**
     * Instantiates a new Find users by name use case.
     *
     * @param findUsersByNameGateway the find users by name gateway
     */
    public FindUsersByNameUseCase(FindUsersByName findUsersByNameGateway) {
        this.findUsersByNameGateway = findUsersByNameGateway;
    }

    /**
     * Execute list.
     *
     * @param name the name
     * @return the list
     */
    public List<UserEntity> execute(String name) {
        log.info("execute: {}", name);
        List<UserEntity> entities = findUsersByNameGateway.findByName(name);
        if (entities == null || entities.isEmpty())
            throw new UserNotFoundException();

        return entities;
    }
}
