package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Find users by cpf list use case.
 */
public class FindUsersByCpfListUseCase {
    private final Logger log = LoggerFactory.getLogger(FindUsersByCpfListUseCase.class);
    private final FindUsersByCpfList findUsersByCpfListGateway;
    private static final String CPF_LIST_CANNOT_BE_NULL_OR_EMPTY = "Cpf list cannot be null or empty";

    /**
     * Instantiates a new Find users by cpf list use case.
     *
     * @param findUsersByCpfListGateway the find users by cpf list gateway
     */
    public FindUsersByCpfListUseCase(FindUsersByCpfList findUsersByCpfListGateway) {
        this.findUsersByCpfListGateway = findUsersByCpfListGateway;
    }

    /**
     * Execute list.
     *
     * @param cpfList the cpf list
     * @return the list
     */
    public List<UserEntity> execute(List<String> cpfList) {
        log.info("execute: {}", cpfList);
        Assert.notEmpty(cpfList, CPF_LIST_CANNOT_BE_NULL_OR_EMPTY);

        return findUsersByCpfListGateway.findByCpfIn(cpfList);
    }
}
