package com.benparvar.ccrest.usecase.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Activate user before create use case.
 */
public class ActivateUserBeforeCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ActivateUserBeforeCreateUseCase.class);

    private static final String USER_CANNOT_BE_NULL = "UserEntity cannot be null";

    /**
     * Execute user entity.
     *
     * @param userEntity the user entity
     * @return the user entity
     */
    public UserEntity execute(UserEntity userEntity) {
        log.info("execute: {}", userEntity);
        Assert.notNull(userEntity, USER_CANNOT_BE_NULL);
        return UserEntity.newBuilder()
                .id(userEntity.getId())
                .cpf(userEntity.getCpf())
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .birthDate(userEntity.getBirthDate())
                .address(userEntity.getAddress())
                .phones(userEntity.getPhones())
                .emails(userEntity.getEmails())
                .active(Boolean.TRUE)
                .build();
    }
}
