package com.benparvar.ccrest.usecase.read.users.name;

import com.benparvar.ccrest.domain.entity.UserEntity;

import java.util.List;

/**
 * The interface Find users by name.
 */
public interface FindUsersByName {

    /**
     * Find by name list.
     *
     * @param name the name
     * @return the list
     */
    List<UserEntity> findByName(String name);
}
