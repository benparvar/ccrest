package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Separate inactive users use case.
 */
public class SeparateInactiveUsersUseCase {
    private final Logger log = LoggerFactory.getLogger(SeparateInactiveUsersUseCase.class);

    private static final String USER_ENTITY_LIST_CANNOT_BE_NULL = "User entity list cannot be null";

    /**
     * Execute list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<UserEntity> execute(List<UserEntity> entities) {
        log.info("execute: {}", entities);
        Assert.notNull(entities, USER_ENTITY_LIST_CANNOT_BE_NULL);

        return entities.stream()
                .filter(e -> Boolean.FALSE.equals(e.getActive()))
                .collect(Collectors.toList());
    }
}
