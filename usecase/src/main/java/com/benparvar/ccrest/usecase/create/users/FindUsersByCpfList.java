package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;

import java.util.List;

/**
 * The interface Find users by cpf list.
 */
public interface FindUsersByCpfList {
    /**
     * Find by cpf in list.
     *
     * @param cpfList the cpf list
     * @return the list
     */
    List<UserEntity> findByCpfIn(List<String> cpfList);
}
