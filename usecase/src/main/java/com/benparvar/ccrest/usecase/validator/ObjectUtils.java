package com.benparvar.ccrest.usecase.validator;

/**
 * The type Object utils.
 */
public interface ObjectUtils {
    /**
     * Is empty boolean.
     *
     * @param array the array
     * @return the boolean
     */
    public static boolean isEmpty(Object[] array) {
        return (array == null || array.length == 0);
    }
}
