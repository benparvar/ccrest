package com.benparvar.ccrest.usecase.delete;

import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Delete user use case.
 */
public class DeleteUserUseCase {
    private final Logger log = LoggerFactory.getLogger(DeleteUserUseCase.class);

    private final DeleteUser deleteUserGateway;
    private final ValidateCpfBeforeDeleteUserUseCase validateCpfBeforeDeleteUserUseCase;

    /**
     * Instantiates a new Delete user use case.
     *
     * @param validateCpfBeforeDeleteUserUseCase the validate cpf before delete user use case
     * @param deleteUserGateway                  the delete user gateway
     */
    public DeleteUserUseCase(ValidateCpfBeforeDeleteUserUseCase validateCpfBeforeDeleteUserUseCase,
                             DeleteUser deleteUserGateway) {
        this.deleteUserGateway = deleteUserGateway;
        this.validateCpfBeforeDeleteUserUseCase = validateCpfBeforeDeleteUserUseCase;
    }

    /**
     * Execute.
     *
     * @param cpf the cpf
     * @return the int
     */
    public int execute(String cpf) {
        log.info("execute: {}", cpf);

        validateCpfBeforeDeleteUserUseCase.execute(cpf);
        int deletedUsers = deleteUserGateway.deleteUsingCpf(cpf);

        if (deletedUsers == 0)
            throw new UserNotFoundException();

        return deletedUsers;
    }
}
