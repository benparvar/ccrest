package com.benparvar.ccrest.usecase.read.user.cpf;

import com.benparvar.ccrest.domain.entity.UserEntity;

/**
 * The interface Find user by cpf.
 */
public interface FindUserByCpf {

    /**
     * Find by cpf user entity.
     *
     * @param cpf the cpf
     * @return the user entity
     */
    UserEntity findByCpf(String cpf);
}
