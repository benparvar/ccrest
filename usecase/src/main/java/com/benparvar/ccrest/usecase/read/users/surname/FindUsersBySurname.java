package com.benparvar.ccrest.usecase.read.users.surname;

import com.benparvar.ccrest.domain.entity.UserEntity;

import java.util.List;

/**
 * The interface Find users by surname.
 */
public interface FindUsersBySurname {
    /**
     * Find by surname list.
     *
     * @param surname the surname
     * @return the list
     */
    List<UserEntity> findBySurname(String surname);
}
