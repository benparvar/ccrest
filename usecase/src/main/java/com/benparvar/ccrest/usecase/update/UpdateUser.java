package com.benparvar.ccrest.usecase.update;

import com.benparvar.ccrest.domain.entity.UserEntity;

/**
 * The interface Update user.
 */
public interface UpdateUser {
    /**
     * Update user int.
     *
     * @param userEntity the user entity
     * @return the int
     */
    int updateUser(UserEntity userEntity);
}
