package com.benparvar.ccrest.usecase.update;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.domain.entity.UserEntityToUpdate;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Update user use case.
 */
public class UpdateUserUseCase {
    private final Logger log = LoggerFactory.getLogger(UpdateUserUseCase.class);

    private final ValidateCpfBeforeUpdateUserUseCase validateCpfBeforeUpdateUserUseCase;
    private final ValidateUserBeforeUpdateUseCase validateUserBeforeUpdateUseCase;
    private final ValidateUserToUpdateBeforeTransformToUserUseCase validateUserToUpdateBeforeTransformToUserUseCase;
    private final UpdateUser updateUserGateway;

    /**
     * Instantiates a new Update user use case.
     *
     * @param validateCpfBeforeUpdateUserUseCase               the validate cpf before update user use case
     * @param validateUserBeforeUpdateUseCase                  the validate user before update use case
     * @param validateUserToUpdateBeforeTransformToUserUseCase the validate user to update before transform to user
     * @param updateUserGateway                                the update user gateway
     */
    public UpdateUserUseCase(ValidateCpfBeforeUpdateUserUseCase validateCpfBeforeUpdateUserUseCase,
                             ValidateUserBeforeUpdateUseCase validateUserBeforeUpdateUseCase,
                             ValidateUserToUpdateBeforeTransformToUserUseCase validateUserToUpdateBeforeTransformToUserUseCase,
                             UpdateUser updateUserGateway) {
        this.validateCpfBeforeUpdateUserUseCase = validateCpfBeforeUpdateUserUseCase;
        this.validateUserBeforeUpdateUseCase = validateUserBeforeUpdateUseCase;
        this.validateUserToUpdateBeforeTransformToUserUseCase = validateUserToUpdateBeforeTransformToUserUseCase;
        this.updateUserGateway = updateUserGateway;
    }

    /**
     * Execute int.
     *
     * @param userEntityToUpdate the user entity to update
     * @param cpf                the cpf
     * @return the int
     */
    public int execute(UserEntityToUpdate userEntityToUpdate, String cpf) {
        log.info("execute: cpf {} - userEntityToUpdate {}", cpf, userEntityToUpdate);
        validateCpfBeforeUpdateUserUseCase.execute(cpf);
        validateUserToUpdateBeforeTransformToUserUseCase.execute(userEntityToUpdate);

        UserEntity userEntity = UserEntity.newBuilder()
                .cpf(cpf)
                .name(userEntityToUpdate.getName())
                .surname(userEntityToUpdate.getSurname())
                .address(userEntityToUpdate.getAddress())
                .birthDate(userEntityToUpdate.getBirthDate())
                .active(userEntityToUpdate.getActive())
                .emails(userEntityToUpdate.getEmails())
                .phones(userEntityToUpdate.getPhones())
                .build();

        validateUserBeforeUpdateUseCase.execute(userEntity);

        int updatedUser = updateUserGateway.updateUser(userEntity);

        if (updatedUser == 0)
            throw new UserNotFoundException();

        return updatedUser;
    }
}
