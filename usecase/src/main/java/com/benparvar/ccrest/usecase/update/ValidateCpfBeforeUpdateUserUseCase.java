package com.benparvar.ccrest.usecase.update;

import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate cpf before update user use case.
 */
public class ValidateCpfBeforeUpdateUserUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateCpfBeforeUpdateUserUseCase.class);

    private static final String CPF_CANNOT_BE_EMPTY = "CPF cannot be null or empty";
    private static final String CPF_MUST_BE_VALID = "CPF must be valid";
    private static final String CPF_REGEX = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)";

    /**
     * Execute boolean.
     *
     * @param cpf the cpf
     * @return the boolean
     */
    public boolean execute(String cpf) {
        log.info("execute: {}", cpf);

        Assert.hasText(cpf, CPF_CANNOT_BE_EMPTY);
        Assert.isTrue(cpf.matches(CPF_REGEX), CPF_MUST_BE_VALID);

        return true;
    }
}
