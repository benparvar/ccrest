package com.benparvar.ccrest.usecase.exception;

/**
 * The type User already exists exception.
 */
public class UserAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = 4940013168256615797L;
}
