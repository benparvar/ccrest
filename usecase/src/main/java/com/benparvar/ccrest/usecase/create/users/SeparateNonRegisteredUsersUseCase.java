package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Separate non registered users use case.
 */
public class SeparateNonRegisteredUsersUseCase {
    private final Logger log = LoggerFactory.getLogger(SeparateNonRegisteredUsersUseCase.class);

    private static final String USER_ENTITY_LIST_CANNOT_BE_NULL = "User entity list cannot be null";
    private static final String ALREADY_REGISTERED_USER_ENTITY_LIST_CANNOT_BE_NULL = "Already registered user entity list cannot be null";

    /**
     * Execute list.
     *
     * @param entities               the entities
     * @param alreadyRegisteredUsers the already registered users
     * @return the list
     */
    public List<UserEntity> execute(List<UserEntity> entities, List<UserEntity> alreadyRegisteredUsers) {
        log.info("execute: {} {}", entities, alreadyRegisteredUsers);
        Assert.notNull(entities, USER_ENTITY_LIST_CANNOT_BE_NULL);
        Assert.notNull(alreadyRegisteredUsers, ALREADY_REGISTERED_USER_ENTITY_LIST_CANNOT_BE_NULL);

        List<String> cpfList = alreadyRegisteredUsers.stream()
                .map(UserEntity::getCpf)
                .collect(Collectors.toList());

        return entities.stream()
                .filter(e -> !cpfList.contains(e.getCpf()))
                .collect(Collectors.toList());
    }
}
