package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;

/**
 * The type Validate users before create use case.
 */
public class ValidateUsersBeforeCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateUsersBeforeCreateUseCase.class);

    private static final String USER_LIST_CANNOT_BE_NULL_OR_EMPTY = "UserEntity list cannot be null or empty";
    private static final String USER_LIST_CANNOT_HAVE_NULL_USER = "List cannot have any null user";

    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_CPF = "List cannot have any empty or null CPF";
    private static final String EVERY_CPF_MUST_BE_VALID = "Every CPF on the list must be valid";
    private static final String CPF_REGEX = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)";

    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_NAME = "List cannot have any empty or null name";
    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_SURNAME = "List cannot have any empty or null surname";

    private static final String USER_LIST_CANNOT_HAVE_NULL_BIRTH_DATE = "List cannot have any empty or null birth date";
    private static final String USER_LIST_CANNOT_HAVE_INVALID_BIRTH_DATE = "List cannot have any invalid birth date";

    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_ADDRESS = "List cannot have any empty or null address";

    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_PHONE_LIST = "List cannot have any empty or null phone list";
    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_PHONE_IN_THE_LIST = "List cannot have any empty or null phone in the list";

    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_EMAIL_LIST = "List cannot have any empty or null email list";
    private static final String USER_LIST_CANNOT_HAVE_NULL_EMPTY_EMAIL_IN_THE_LIST = "List cannot have any empty or null email in the list";

    /**
     * Execute boolean.
     *
     * @param userEntities the user entities
     * @return the boolean
     */
    public Boolean execute(List<UserEntity> userEntities) {
        log.info("execute: {}", userEntities);
        Assert.notEmpty(userEntities, USER_LIST_CANNOT_BE_NULL_OR_EMPTY);

        userEntities.forEach(this::isNullEntity);
        userEntities.forEach(this::isNullOrEmptyCpf);
        userEntities.forEach(this::isInvalidCpf);
        userEntities.forEach(this::isNullOrEmptyName);
        userEntities.forEach(this::isNullOrEmptySurname);
        userEntities.forEach(this::isNullBirthDate);
        userEntities.forEach(this::isInvalidBirthDate);
        userEntities.forEach(this::isNullOrEmptyAddress);
        userEntities.forEach(this::isNullOrEmptyPhoneList);
        userEntities.forEach(this::isNullOrEmptyPhoneInTheList);
        userEntities.forEach(this::isNullOrEmptyEmailList);
        userEntities.forEach(this::isNullOrEmptyEmailInTheList);

        return Boolean.TRUE;
    }

    private void isNullOrEmptyEmail(String email) {
        Assert.hasText(email, USER_LIST_CANNOT_HAVE_NULL_EMPTY_EMAIL_IN_THE_LIST);
    }

    private void isNullOrEmptyEmailInTheList(UserEntity userEntity) {
        Assert.notEmpty(userEntity.getEmails(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_EMAIL_LIST);
        userEntity.getEmails().forEach(this::isNullOrEmptyEmail);
    }

    private void isNullOrEmptyEmailList(UserEntity userEntity) {
        Assert.notEmpty(userEntity.getEmails(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_EMAIL_LIST);
    }

    private void isNullOrEmptyPhoneInTheList(UserEntity userEntity) {
        Assert.notEmpty(userEntity.getPhones(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_PHONE_LIST);
        userEntity.getPhones().forEach(this::isNullOrEmptyPhone);
    }

    private void isNullOrEmptyPhone(String phone) {
        Assert.hasText(phone, USER_LIST_CANNOT_HAVE_NULL_EMPTY_PHONE_IN_THE_LIST);
    }

    private void isNullOrEmptyPhoneList(UserEntity userEntity) {
        Assert.notEmpty(userEntity.getPhones(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_PHONE_LIST);
    }

    private void isNullOrEmptyAddress(UserEntity userEntity) {
        Assert.hasText(userEntity.getAddress(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_ADDRESS);
    }

    private void isInvalidBirthDate(UserEntity userEntity) {
        Assert.isTrue(userEntity.getBirthDate().isBefore(LocalDate.now()), USER_LIST_CANNOT_HAVE_INVALID_BIRTH_DATE);
    }

    private void isNullBirthDate(UserEntity userEntity) {
        Assert.notNull(userEntity.getBirthDate(), USER_LIST_CANNOT_HAVE_NULL_BIRTH_DATE);
    }

    private void isNullOrEmptySurname(UserEntity userEntity) {
        Assert.hasText(userEntity.getSurname(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_SURNAME);
    }

    private void isNullOrEmptyName(UserEntity userEntity) {
        Assert.hasText(userEntity.getName(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_NAME);
    }

    private void isInvalidCpf(UserEntity userEntity) {
        Assert.isTrue(userEntity.getCpf().matches(CPF_REGEX), EVERY_CPF_MUST_BE_VALID);
    }

    private void isNullOrEmptyCpf(UserEntity userEntity) {
        Assert.hasText(userEntity.getCpf(), USER_LIST_CANNOT_HAVE_NULL_EMPTY_CPF);
    }

    private void isNullEntity(UserEntity userEntity) {
        Assert.notNull(userEntity, USER_LIST_CANNOT_HAVE_NULL_USER);
    }
}
