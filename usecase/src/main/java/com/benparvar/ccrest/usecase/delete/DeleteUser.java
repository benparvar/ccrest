package com.benparvar.ccrest.usecase.delete;

/**
 * The interface Delete user.
 */
public interface DeleteUser {
    /**
     * Delete using cpf.
     *
     * @param cpf the cpf
     * @return the int
     */
    int deleteUsingCpf(String cpf);
}
