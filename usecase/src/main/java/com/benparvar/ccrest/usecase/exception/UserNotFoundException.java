package com.benparvar.ccrest.usecase.exception;

/**
 * The type User not found exception.
 */
public class UserNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1201994887682475586L;
}
