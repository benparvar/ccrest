package com.benparvar.ccrest.usecase.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;

/**
 * The interface Save user.
 */
public interface SaveUser {
    /**
     * Save user user entity.
     *
     * @param userEntity the user entity
     * @return the user entity
     */
    UserEntity saveUser(UserEntity userEntity);
}
