package com.benparvar.ccrest.usecase.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

/**
 * The type Validate user before create use case.
 */
public class ValidateUserBeforeCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateUserBeforeCreateUseCase.class);

    private static final String USER_CANNOT_BE_NULL = "UserEntity cannot be null";

    private static final String CPF_CANNOT_BE_EMPTY = "CPF cannot be null or empty";
    private static final String CPF_MUST_BE_VALID = "CPF must be valid";
    private static final String CPF_REGEX = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)";

    private static final String NAME_CANNOT_BE_EMPTY = "Name cannot be null or empty";

    private static final String SURNAME_CANNOT_BE_EMPTY = "Surname cannot be null or empty";

    private static final String BIRTH_DATE_CANNOT_BE_NULL = "Birth date cannot be null";
    private static final String BIRTH_DATE_MUST_BE_VALID = "Birth date must be valid";

    private static final String ADDRESS_CANNOT_BE_EMPTY = "Address cannot be null or empty";

    private static final String PHONES_CANNOT_BE_EMPTY = "Phones cannot be null or empty";

    private static final String EMAILS_CANNOT_BE_EMPTY = "Emails cannot be null or empty";

    /**
     * Execute boolean.
     *
     * @param userEntity the user entity
     * @return the boolean
     */
    public boolean execute(UserEntity userEntity) {
        log.info("execute: {}", userEntity);
        Assert.notNull(userEntity, USER_CANNOT_BE_NULL);

        Assert.hasText(userEntity.getCpf(), CPF_CANNOT_BE_EMPTY);
        Assert.isTrue(userEntity.getCpf().matches(CPF_REGEX), CPF_MUST_BE_VALID);

        Assert.hasText(userEntity.getName(), NAME_CANNOT_BE_EMPTY);

        Assert.hasText(userEntity.getSurname(), SURNAME_CANNOT_BE_EMPTY);

        Assert.notNull(userEntity.getBirthDate(), BIRTH_DATE_CANNOT_BE_NULL);
        Assert.isTrue(userEntity.getBirthDate().isBefore(LocalDate.now()), BIRTH_DATE_MUST_BE_VALID);

        Assert.hasText(userEntity.getAddress(), ADDRESS_CANNOT_BE_EMPTY);

        Assert.notEmpty(userEntity.getPhones(), PHONES_CANNOT_BE_EMPTY);

        Assert.notEmpty(userEntity.getEmails(), EMAILS_CANNOT_BE_EMPTY);

        return true;
    }
}
