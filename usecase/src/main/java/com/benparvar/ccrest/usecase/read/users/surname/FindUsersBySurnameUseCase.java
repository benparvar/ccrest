package com.benparvar.ccrest.usecase.read.users.surname;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Find users by surname use case.
 */
public class FindUsersBySurnameUseCase {
    private final Logger log = LoggerFactory.getLogger(FindUsersBySurnameUseCase.class);

    private final FindUsersBySurname findUsersBySurnameGateway;

    /**
     * Instantiates a new Find users by name use case.
     *
     * @param findUsersBySurnameGateway the find users by surname gateway
     */
    public FindUsersBySurnameUseCase(FindUsersBySurname findUsersBySurnameGateway) {
        this.findUsersBySurnameGateway = findUsersBySurnameGateway;
    }

    /**
     * Execute list.
     *
     * @param surname the surname
     * @return the list
     */
    public List<UserEntity> execute(String surname) {
        log.info("execute: {}", surname);
        List<UserEntity> entities = findUsersBySurnameGateway.findBySurname(surname);
        if (entities == null || entities.isEmpty())
            throw new UserNotFoundException();

        return entities;
    }
}
