package com.benparvar.ccrest.usecase.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Create user use case.
 */
public class CreateUserUseCase {
    private final Logger log = LoggerFactory.getLogger(CreateUserUseCase.class);

    private final ValidateUserBeforeCreateUseCase validateUserBeforeCreateUseCase;
    private final ActivateUserBeforeCreateUseCase activateUserBeforeCreateUseCase;
    private final SaveUser saveUserGateway;

    /**
     * Instantiates a new Create user use case.
     *
     * @param validateUserBeforeCreateUseCase the validate user before create use case
     * @param activateUserBeforeCreateUseCase the activate user before create use case
     * @param saveUserGateway                 the save user gateway
     */
    public CreateUserUseCase(ValidateUserBeforeCreateUseCase validateUserBeforeCreateUseCase, ActivateUserBeforeCreateUseCase activateUserBeforeCreateUseCase,
                             SaveUser saveUserGateway) {
        this.validateUserBeforeCreateUseCase = validateUserBeforeCreateUseCase;
        this.activateUserBeforeCreateUseCase = activateUserBeforeCreateUseCase;
        this.saveUserGateway = saveUserGateway;
    }

    /**
     * Execute user entity.
     *
     * @param userEntity the user entity
     * @return the user entity
     */
    public UserEntity execute(UserEntity userEntity) {
        log.info("execute: {}", userEntity);

        validateUserBeforeCreateUseCase.execute(userEntity);

        return saveUserGateway.saveUser(activateUserBeforeCreateUseCase.execute(userEntity));
    }
}
