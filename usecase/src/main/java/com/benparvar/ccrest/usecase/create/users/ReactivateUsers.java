package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;

import java.util.List;

/**
 * The interface Reactivate users.
 */
public interface ReactivateUsers {
    /**
     * Reactivate int.
     *
     * @param userEntities the user entities
     * @return the int
     */
    int reactivate(List<UserEntity> userEntities);
}
