package com.benparvar.ccrest.usecase.update;

import com.benparvar.ccrest.domain.entity.UserEntityToUpdate;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

/**
 * The type Validate user to update before transform to user.
 */
public class ValidateUserToUpdateBeforeTransformToUserUseCase {

    private final Logger log = LoggerFactory.getLogger(ValidateUserToUpdateBeforeTransformToUserUseCase.class);

    private static final String USER_CANNOT_BE_NULL = "UserEntity cannot be null";

    private static final String NAME_CANNOT_BE_EMPTY = "Name cannot be null or empty";

    private static final String SURNAME_CANNOT_BE_EMPTY = "Surname cannot be null or empty";

    private static final String BIRTH_DATE_CANNOT_BE_NULL = "Birth date cannot be null";
    private static final String BIRTH_DATE_MUST_BE_VALID = "Birth date must be valid";

    private static final String ADDRESS_CANNOT_BE_EMPTY = "Address cannot be null or empty";

    private static final String PHONES_CANNOT_BE_EMPTY = "Phones cannot be null or empty";

    private static final String EMAILS_CANNOT_BE_EMPTY = "Emails cannot be null or empty";

    private static final String ACTIVE_CANNOT_BE_NULL = "Active cannot be null";

    /**
     * Execute boolean.
     *
     * @param userEntityToUpdate the user entity
     * @return the boolean
     */
    public boolean execute(UserEntityToUpdate userEntityToUpdate) {
        log.info("execute: {}", userEntityToUpdate);
        Assert.notNull(userEntityToUpdate, USER_CANNOT_BE_NULL);

        Assert.hasText(userEntityToUpdate.getName(), NAME_CANNOT_BE_EMPTY);

        Assert.hasText(userEntityToUpdate.getSurname(), SURNAME_CANNOT_BE_EMPTY);

        Assert.notNull(userEntityToUpdate.getBirthDate(), BIRTH_DATE_CANNOT_BE_NULL);
        Assert.isTrue(userEntityToUpdate.getBirthDate().isBefore(LocalDate.now()), BIRTH_DATE_MUST_BE_VALID);

        Assert.hasText(userEntityToUpdate.getAddress(), ADDRESS_CANNOT_BE_EMPTY);

        Assert.notEmpty(userEntityToUpdate.getPhones(), PHONES_CANNOT_BE_EMPTY);

        Assert.notEmpty(userEntityToUpdate.getEmails(), EMAILS_CANNOT_BE_EMPTY);

        Assert.notNull(userEntityToUpdate.getActive(), ACTIVE_CANNOT_BE_NULL);

        return true;
    }
}
