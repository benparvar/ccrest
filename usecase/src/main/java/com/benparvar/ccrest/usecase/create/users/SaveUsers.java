package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;

import java.util.List;

/**
 * The interface Save users.
 */
public interface SaveUsers {
    /**
     * Save user list.
     *
     * @param userEntities the user entities
     * @return the list
     */
    List<UserEntity> saveUser(List<UserEntity> userEntities);
}
