module com.benparvar.ccrest.usecase {
    exports com.benparvar.ccrest.usecase.create.user;
    exports com.benparvar.ccrest.usecase.create.users;
    exports com.benparvar.ccrest.usecase.delete;
    exports com.benparvar.ccrest.usecase.read.user.cpf;
    exports com.benparvar.ccrest.usecase.read.users.name;
    exports com.benparvar.ccrest.usecase.read.users.surname;
    exports com.benparvar.ccrest.usecase.update;
    exports com.benparvar.ccrest.usecase.exception;
    exports com.benparvar.ccrest.usecase.validator;

    requires com.benparvar.ccrest.domain;
    requires org.apache.commons.lang3;
    requires org.slf4j;
}
