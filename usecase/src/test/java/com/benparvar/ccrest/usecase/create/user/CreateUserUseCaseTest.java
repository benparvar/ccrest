package com.benparvar.ccrest.usecase.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserAlreadyExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Create user use case test.
 */
public class CreateUserUseCaseTest {

    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final LocalDate VALID_LOCAL_DATE = LocalDate.now().minusYears(20L);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    private final ValidateUserBeforeCreateUseCase validateUserBeforeCreateUseCase = mock(ValidateUserBeforeCreateUseCase.class);
    private final ActivateUserBeforeCreateUseCase activateUserBeforeCreateUseCase = mock(ActivateUserBeforeCreateUseCase.class);
    private final SaveUser saveUser = mock(SaveUser.class);

    private CreateUserUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new CreateUserUseCase(validateUserBeforeCreateUseCase, activateUserBeforeCreateUseCase, saveUser);
    }

    /**
     * Execute with null entity will fail.
     */
    @Test
    public void executeWithNullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = null;

            when(validateUserBeforeCreateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("UserEntity cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("UserEntity cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty entity will fail.
     */
    @Test
    public void executeWithEmptyEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder().build();

            when(validateUserBeforeCreateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("CPF cannot be null or empty"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    @Test
    public void executeWithAPreExistentUserWillFail() {
        assertThrows(UserAlreadyExistsException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(VALID_EMAILS)
                    .build();

            UserEntity activedEntity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(VALID_EMAILS)
                    .active(VALID_ACTIVE_TRUE)
                    .build();

            UserEntity persistedEntity = UserEntity.newBuilder()
                    .id(VALID_ID)
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(VALID_EMAILS)
                    .active(VALID_ACTIVE_TRUE)
                    .build();

            when(validateUserBeforeCreateUseCase.execute(entity)).thenReturn(Boolean.TRUE);
            when(activateUserBeforeCreateUseCase.execute(entity)).thenReturn(activedEntity);
            when(saveUser.saveUser(activedEntity)).thenThrow(new UserAlreadyExistsException());

            try {
                uc.execute(entity);
            } catch (UserAlreadyExistsException e) {
                throw e;
            }

            verify(validateUserBeforeCreateUseCase, times(1)).execute(entity);
            verify(activateUserBeforeCreateUseCase, times(1)).execute(entity);
            verify(saveUser, times(1)).saveUser(activedEntity);

            fail("should throw an UserAlreadyExistsException");
        });
    }

    /**
     * Execute with true active user will success.
     */
    @Test
    public void executeWithTrueActiveUserWillSuccess() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .build();

        UserEntity activedEntity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();

        UserEntity persistedEntity = UserEntity.newBuilder()
                .id(VALID_ID)
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();

        when(validateUserBeforeCreateUseCase.execute(entity)).thenReturn(Boolean.TRUE);
        when(activateUserBeforeCreateUseCase.execute(entity)).thenReturn(activedEntity);
        when(saveUser.saveUser(activedEntity)).thenReturn(persistedEntity);

        UserEntity result = uc.execute(entity);

        assertNotNull(result);
        assertTrue(result.getActive(), "User must be active");
        assertNotNull(result.getId(), "User must have a Id");

        assertEquals(persistedEntity, result);
    }
}
