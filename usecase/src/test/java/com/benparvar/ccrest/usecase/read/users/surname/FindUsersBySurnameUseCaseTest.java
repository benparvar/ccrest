package com.benparvar.ccrest.usecase.read.users.surname;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find users by surname use case test.
 */
class FindUsersBySurnameUseCaseTest {
    private final FindUsersBySurname findUsersBySurname = mock(FindUsersBySurname.class);
    private FindUsersBySurnameUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindUsersBySurnameUseCase(findUsersBySurname);
    }

    /**
     * Execute with a non existent name will fail.
     */
    @Test
    public void executeWithANonExistentNameWillFail() {
        assertThrows(UserNotFoundException.class, () -> {
            String name = "Xablau";

            when(findUsersBySurname.findBySurname(name)).thenReturn(Collections.emptyList());

            try {
                uc.execute(name);
            } catch (UserNotFoundException e) {
                verify(findUsersBySurname, times(1)).findBySurname(name);
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute with a existent name will success.
     */
    @Test
    public void executeWithAExistentNameWillSuccess() {
        String name = "Xemele";

        when(findUsersBySurname.findBySurname(name)).thenReturn(Arrays.asList(UserEntity.newBuilder().name(name).build()));

        List<UserEntity> entities = uc.execute(name);
        assertNotNull(entities);
        assertEquals(1, entities.size());
        assertEquals(name, entities.get(0).getName());

        verify(findUsersBySurname, times(1)).findBySurname(name);

    }
}