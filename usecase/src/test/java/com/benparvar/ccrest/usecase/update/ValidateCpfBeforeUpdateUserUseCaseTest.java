package com.benparvar.ccrest.usecase.update;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate cpf before update user use case test.
 */
class ValidateCpfBeforeUpdateUserUseCaseTest {
    private final String VALID_CPF = "324.880.350-92";

    private ValidateCpfBeforeUpdateUserUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateCpfBeforeUpdateUserUseCase();
    }

    /**
     * Execute with null cpf will fail.
     */
    @Test
    public void executeWithNullCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = null;

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty cpf will fail.
     */
    @Test
    public void executeWithEmptyCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "";

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with invalid alpha numeric cpf will fail.
     */
    @Test
    public void executeWithInvalidAlphaNumericCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "abc123";

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with invalid alpha cpf will fail.
     */
    @Test
    public void executeWithInvalidAlphaCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "abcasd";

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with invalid numeric cpf will fail.
     */
    @Test
    public void executeWithInvalidNumericCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "11143211111";

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with false active user will success.
     */
    @Test
    public void executeWithFalseActiveUserWillSuccess() {
        String cpf = VALID_CPF;

        assertTrue(uc.execute(cpf), "Must be a valid inactive user");
    }
}