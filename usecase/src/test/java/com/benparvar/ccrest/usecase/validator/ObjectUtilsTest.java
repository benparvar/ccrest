package com.benparvar.ccrest.usecase.validator;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The type Object utils test.
 */
public class ObjectUtilsTest {

    /**
     * Execute with a null array will return true.
     */
    @Test
    public void executeWithANullArrayWillReturnTrue() {
        String[] list = null;

        boolean result = ObjectUtils.isEmpty(list);
        assertTrue(result);
    }

    /**
     * Execute with a empty array will return true.
     */
    @Test
    public void executeWithAEmptyArrayWillReturnTrue() {
        List<String> list = Collections.EMPTY_LIST;

        boolean result = ObjectUtils.isEmpty(list.toArray());
        assertTrue(result);
    }

    /**
     * Execute with an valid array will return false.
     */
    @Test
    public void executeWithANValidArrayWillReturnFalse() {
        List<String> list = Arrays.asList("RoykSopp");

        boolean result = ObjectUtils.isEmpty(list.toArray());
        assertFalse(result);
    }
}
