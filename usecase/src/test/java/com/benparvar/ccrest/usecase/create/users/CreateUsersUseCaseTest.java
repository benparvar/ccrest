package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The type Create users use case test.
 */
class CreateUsersUseCaseTest {
    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final LocalDate VALID_LOCAL_DATE = LocalDate.now().minusYears(20L);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;
    private CreateUsersUseCase uc;
    private final SaveUsers saveUsersGateway = mock(SaveUsers.class);
    private final FindUsersByCpfList findUsersByCpfListGateway = mock(FindUsersByCpfList.class);
    private final ValidateUsersBeforeCreateUseCase validateUsersBeforeCreateUseCase = mock(ValidateUsersBeforeCreateUseCase.class);
    private final SeparateActiveUsersUseCase separateActiveUsersUseCase = mock(SeparateActiveUsersUseCase.class);
    private final SeparateInactiveUsersUseCase separateInactiveUsersUseCase = mock(SeparateInactiveUsersUseCase.class);
    private final SeparateNonRegisteredUsersUseCase separateNonRegisteredUsersUseCase = mock(SeparateNonRegisteredUsersUseCase.class);
    private final ActivateUsersBeforeCreateUseCase activateUsersBeforeCreateUseCase = mock(ActivateUsersBeforeCreateUseCase.class);
    private final ActivateUsersAfterCreateUseCase activateUsersAfterCreateUseCase = mock(ActivateUsersAfterCreateUseCase.class);
    private final ReactivateUsers reactivateUsersGateway = mock(ReactivateUsers.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new CreateUsersUseCase(validateUsersBeforeCreateUseCase, separateActiveUsersUseCase,
                separateInactiveUsersUseCase, separateNonRegisteredUsersUseCase, activateUsersBeforeCreateUseCase,
                activateUsersAfterCreateUseCase, findUsersByCpfListGateway, reactivateUsersGateway, saveUsersGateway);
    }

    /**
     * Execute with empty user list will success.
     */
    @Test
    public void executeWithEmptyUserListWillSuccess() {
        List<UserEntity> entities = Collections.emptyList();
        List<UserEntity> persistedEntities = Collections.emptyList();

        when(activateUsersAfterCreateUseCase.execute(entities)).thenReturn(persistedEntities);

        List<UserEntity> result = uc.execute(entities);

        assertNotNull(result);
        assertEquals(persistedEntities, result);
    }

    /**
     * Execute with true active user list will success.
     */
    @Test
    public void executeWithTrueActiveUserListWillSuccess() {
        List<UserEntity> entities = Arrays.asList(UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .build());

        List<UserEntity> persistedEntities = Arrays.asList(UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build());

        when(activateUsersAfterCreateUseCase.execute(entities)).thenReturn(persistedEntities);

        List<UserEntity> result = uc.execute(entities);

        assertNotNull(result);
        assertEquals(persistedEntities, result);
    }
}