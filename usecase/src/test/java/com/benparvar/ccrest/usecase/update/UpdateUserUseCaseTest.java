package com.benparvar.ccrest.usecase.update;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.domain.entity.UserEntityToUpdate;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Update user use case test.
 */
class UpdateUserUseCaseTest {

    private final ValidateCpfBeforeUpdateUserUseCase validateCpfBeforeUpdateUserUseCase = mock(ValidateCpfBeforeUpdateUserUseCase.class);
    private final ValidateUserBeforeUpdateUseCase validateUserBeforeUpdateUseCase = mock(ValidateUserBeforeUpdateUseCase.class);
    private final ValidateUserToUpdateBeforeTransformToUserUseCase validateUserToUpdateBeforeTransformToUserUseCase = mock(ValidateUserToUpdateBeforeTransformToUserUseCase.class);
    private final UpdateUser updateUser = mock(UpdateUser.class);

    private UpdateUserUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new UpdateUserUseCase(validateCpfBeforeUpdateUserUseCase, validateUserBeforeUpdateUseCase,
                validateUserToUpdateBeforeTransformToUserUseCase, updateUser);
    }

    /**
     * Execute with null cpf and null entity will fail.
     */
    @Test
    public void executeWithNullCPFAndNullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = null;
            UserEntityToUpdate entityToUpdate = null;

            when(validateCpfBeforeUpdateUserUseCase.execute(cpf))
                    .thenThrow(new IllegalArgumentException("CPF cannot be null"));
            when(validateUserToUpdateBeforeTransformToUserUseCase.execute(entityToUpdate))
                    .thenThrow(new IllegalArgumentException("Entity cannot be null"));

            try {
                uc.execute(entityToUpdate, cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null", e.getMessage());
                verify(validateCpfBeforeUpdateUserUseCase, times(1)).execute(cpf);
                verify(validateUserToUpdateBeforeTransformToUserUseCase, times(0)).execute(any());
                verify(updateUser, times(0)).updateUser(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty cpf and null entity will fail.
     */
    @Test
    public void executeWithEmptyCPFAndNullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "";
            UserEntityToUpdate entityToUpdate = null;

            when(validateCpfBeforeUpdateUserUseCase.execute(cpf))
                    .thenThrow(new IllegalArgumentException("CPF cannot be empty"));
            when(validateUserToUpdateBeforeTransformToUserUseCase.execute(entityToUpdate))
                    .thenThrow(new IllegalArgumentException("Entity cannot be null"));

            try {
                uc.execute(entityToUpdate, cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be empty", e.getMessage());
                verify(validateCpfBeforeUpdateUserUseCase, times(1)).execute(cpf);
                verify(validateUserToUpdateBeforeTransformToUserUseCase, times(0)).execute(any());
                verify(updateUser, times(0)).updateUser(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with valid cpf and null entity will fail.
     */
    @Test
    public void executeWithValidCPFAndNullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "005.573.950-46";
            UserEntityToUpdate entityToUpdate = null;

            when(validateCpfBeforeUpdateUserUseCase.execute(cpf))
                    .thenReturn(Boolean.TRUE);
            when(validateUserToUpdateBeforeTransformToUserUseCase.execute(entityToUpdate))
                    .thenThrow(new IllegalArgumentException("Entity cannot be null"));

            try {
                uc.execute(entityToUpdate, cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("Entity cannot be null", e.getMessage());
                verify(validateCpfBeforeUpdateUserUseCase, times(1)).execute(cpf);
                verify(validateUserToUpdateBeforeTransformToUserUseCase, times(1)).execute(any());
                verify(updateUser, times(0)).updateUser(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with valid cpf and empty entity will fail.
     */
    @Test
    public void executeWithValidCPFAndEmptyEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "005.573.950-46";
            UserEntityToUpdate entityToUpdate = UserEntityToUpdate.newBuilder().build();

            when(validateCpfBeforeUpdateUserUseCase.execute(cpf))
                    .thenReturn(Boolean.TRUE);
            when(validateUserToUpdateBeforeTransformToUserUseCase.execute(entityToUpdate))
                    .thenThrow(new IllegalArgumentException("Name cannot be empty"));

            try {
                uc.execute(entityToUpdate, cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be empty", e.getMessage());
                verify(validateCpfBeforeUpdateUserUseCase, times(1)).execute(cpf);
                verify(validateUserToUpdateBeforeTransformToUserUseCase, times(1)).execute(any());
                verify(updateUser, times(0)).updateUser(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with valid but non existent cpf and valid entity will fail.
     */
    @Test
    public void executeWithValidButNonExistentCPFAndValidEntityWillFail() {
        assertThrows(UserNotFoundException.class, () -> {
            String cpf = "005.573.950-46";
            UserEntityToUpdate entityToUpdate = UserEntityToUpdate.newBuilder().build();
            UserEntity entity = UserEntity.newBuilder().cpf(cpf).build();

            when(validateCpfBeforeUpdateUserUseCase.execute(cpf))
                    .thenReturn(Boolean.TRUE);
            when(validateUserToUpdateBeforeTransformToUserUseCase.execute(entityToUpdate))
                    .thenReturn(Boolean.TRUE);
            when(validateUserBeforeUpdateUseCase.execute(entity)).thenReturn(Boolean.TRUE);
            when(updateUser.updateUser(entity)).thenReturn(0);

            try {
                uc.execute(entityToUpdate, cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be empty", e.getMessage());
                verify(validateCpfBeforeUpdateUserUseCase, times(1)).execute(cpf);
                verify(validateUserToUpdateBeforeTransformToUserUseCase, times(1)).execute(any());
                verify(validateUserBeforeUpdateUseCase, times(1)).execute(entity);
                verify(updateUser, times(1)).updateUser(any());
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute with valid existent cpf and valid entity will fail.
     */
    @Test
    public void executeWithValidExistentCPFAndValidEntityWillFail() {

        String cpf = "005.573.950-46";
        UserEntityToUpdate entityToUpdate = UserEntityToUpdate.newBuilder().build();
        UserEntity entity = UserEntity.newBuilder().cpf(cpf).build();

        when(validateCpfBeforeUpdateUserUseCase.execute(cpf))
                .thenReturn(Boolean.TRUE);
        when(validateUserToUpdateBeforeTransformToUserUseCase.execute(entityToUpdate))
                .thenReturn(Boolean.TRUE);
        when(validateUserBeforeUpdateUseCase.execute(entity)).thenReturn(Boolean.TRUE);
        when(updateUser.updateUser(entity)).thenReturn(1);


        int result = uc.execute(entityToUpdate, cpf);

        assertEquals(1, result);
        verify(validateCpfBeforeUpdateUserUseCase, times(1)).execute(cpf);
        verify(validateUserToUpdateBeforeTransformToUserUseCase, times(1)).execute(any());
        verify(validateUserBeforeUpdateUseCase, times(1)).execute(entity);
        verify(updateUser, times(1)).updateUser(any());
    }

}