package com.benparvar.ccrest.usecase.delete;

import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Delete user use case test.
 */
class DeleteUserUseCaseTest {

    private final DeleteUser deleteUser = mock(DeleteUser.class);
    private final ValidateCpfBeforeDeleteUserUseCase validateCpfBeforeDeleteUserUseCase = mock(ValidateCpfBeforeDeleteUserUseCase.class);

    private DeleteUserUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new DeleteUserUseCase(validateCpfBeforeDeleteUserUseCase, deleteUser);
    }

    /**
     * Execute with null cpf will fail.
     */
    @Test
    public void executeWithNullCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = null;

            when(validateCpfBeforeDeleteUserUseCase.execute(cpf))
                    .thenThrow(new IllegalArgumentException("CPF cannot be null"));

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null", e.getMessage());
                verify(deleteUser, times(0)).deleteUsingCpf(cpf);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty cpf will fail.
     */
    @Test
    public void executeWithEmptyCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "";

            when(validateCpfBeforeDeleteUserUseCase.execute(cpf))
                    .thenThrow(new IllegalArgumentException("CPF cannot be null or empty"));

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                verify(deleteUser, times(0)).deleteUsingCpf(cpf);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a non existent cpf will fail.
     */
    @Test
    public void executeWithANonExistentCPFWillFail() {
        assertThrows(UserNotFoundException.class, () -> {
            String cpf = "905.576.230-06";

            when(validateCpfBeforeDeleteUserUseCase.execute(cpf)).thenReturn(true);
            when(deleteUser.deleteUsingCpf(cpf)).thenReturn(0);

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("", e.getMessage());

                verify(validateCpfBeforeDeleteUserUseCase, times(1)).execute(cpf);
                verify(deleteUser, times(1)).deleteUsingCpf(cpf);
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute with a valid cpf will success.
     */
    @Test
    public void executeWithAValidCPFWillSuccess() {
        String cpf = "005.573.950-46";
        when(validateCpfBeforeDeleteUserUseCase.execute(cpf)).thenReturn(true);
        when(deleteUser.deleteUsingCpf(cpf)).thenReturn(1);

        uc.execute(cpf);

        verify(deleteUser, times(1)).deleteUsingCpf(cpf);
    }
}