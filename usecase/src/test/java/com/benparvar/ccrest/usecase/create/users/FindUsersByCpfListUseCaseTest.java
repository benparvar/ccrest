package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find users by cpf list use case test.
 */
class FindUsersByCpfListUseCaseTest {

    private FindUsersByCpfListUseCase uc;
    private final FindUsersByCpfList findUsersByCpfListGateway = mock(FindUsersByCpfList.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindUsersByCpfListUseCase(findUsersByCpfListGateway);
    }

    /**
     * Execute with null cpf list will fail.
     */
    @Test
    public void executeWithNullCpfListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<String> cpfList = null;

            try {
                uc.execute(cpfList);
            } catch (IllegalArgumentException e) {
                assertEquals("Cpf list cannot be null or empty", e.getMessage());
                verify(findUsersByCpfListGateway, times(0)).findByCpfIn(cpfList);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty cpf list will fail.
     */
    @Test
    public void executeWithEmptyCpfListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<String> cpfList = Collections.emptyList();

            try {
                uc.execute(cpfList);
            } catch (IllegalArgumentException e) {
                assertEquals("Cpf list cannot be null or empty", e.getMessage());
                verify(findUsersByCpfListGateway, times(0)).findByCpfIn(cpfList);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with non existent cpf list will return a empty list.
     */
    @Test
    public void executeWithNonExistentCpfListWillReturnAEmptyList() {
        List<String> cpfList = Arrays.asList("276.19.418-03", "455.848.770-34");

        when(findUsersByCpfListGateway.findByCpfIn(cpfList)).thenReturn(Collections.emptyList());

        List<UserEntity> entities = uc.execute(cpfList);
        assertNotNull(entities, "List must be not null");
        assertTrue(entities.isEmpty());

        verify(findUsersByCpfListGateway, times(1)).findByCpfIn(cpfList);
    }

    /**
     * Execute with an existent cpf list will return a list.
     */
    @Test
    public void executeWithAnExistentCpfListWillReturnAList() {
        String cpf_1 = "276.19.418-03";
        String cpf_2 = "455.848.770-34";
        String cpf_3 = "393.459.210-44";

        List<String> cpfList = Arrays.asList(cpf_1, cpf_2, cpf_3);

        List<UserEntity> gwEntities = Arrays.asList(
                UserEntity.newBuilder().cpf(cpf_1).build(),
                UserEntity.newBuilder().cpf(cpf_3).build(),
                UserEntity.newBuilder().cpf(cpf_3).build()
        );

        when(findUsersByCpfListGateway.findByCpfIn(cpfList)).thenReturn(gwEntities);

        List<UserEntity> entities = uc.execute(cpfList);
        assertNotNull(entities, "List must be not null");
        assertEquals(entities.size(), 3);

        verify(findUsersByCpfListGateway, times(1)).findByCpfIn(cpfList);
    }
}