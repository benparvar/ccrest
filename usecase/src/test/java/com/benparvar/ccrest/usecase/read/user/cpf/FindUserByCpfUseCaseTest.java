package com.benparvar.ccrest.usecase.read.user.cpf;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find user by cpf use case test.
 */
public class FindUserByCpfUseCaseTest {
    private final FindUserByCpf findUserByCpf = mock(FindUserByCpf.class);
    private final ValidateCpfBeforeFindUserUseCase validateCpfBeforeFindUserUseCase = mock(ValidateCpfBeforeFindUserUseCase.class);

    private FindUserByCpfUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindUserByCpfUseCase(validateCpfBeforeFindUserUseCase, findUserByCpf);
    }

    /**
     * Execute with null cpf will fail.
     */
    @Test
    public void executeWithNullCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = null;

            when(validateCpfBeforeFindUserUseCase.execute(cpf))
                    .thenThrow(new IllegalArgumentException("CPF cannot be null"));

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null", e.getMessage());
                verify(findUserByCpf, times(0)).findByCpf(cpf);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty cpf will fail.
     */
    @Test
    public void executeWithEmptyCPFWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "";

            when(validateCpfBeforeFindUserUseCase.execute(cpf))
                    .thenThrow(new IllegalArgumentException("CPF cannot be null or empty"));

            try {
                uc.execute(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                verify(findUserByCpf, times(0)).findByCpf(cpf);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a non existent cpf will fail.
     */
    @Test
    public void executeWithANonExistentCPFWillFail() {
        assertThrows(UserNotFoundException.class, () -> {
            String cpf = "905.576.230-06";

            when(validateCpfBeforeFindUserUseCase.execute(cpf)).thenReturn(true);
            when(findUserByCpf.findByCpf(cpf)).thenReturn(null);

            try {
                uc.execute(cpf);
            } catch (UserNotFoundException e) {
                verify(validateCpfBeforeFindUserUseCase, times(1)).execute(cpf);
                verify(findUserByCpf, times(1)).findByCpf(cpf);
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute with a valid cpf will success.
     */
    @Test
    public void executeWithAValidCPFWillSuccess() {
        String cpf = "005.573.950-46";
        when(validateCpfBeforeFindUserUseCase.execute(cpf)).thenReturn(true);
        when(findUserByCpf.findByCpf(cpf)).thenReturn(UserEntity.newBuilder().cpf(cpf).build());

        UserEntity founded = uc.execute(cpf);
        assertNotNull(founded);
        assertEquals(cpf, founded.getCpf());

        verify(validateCpfBeforeFindUserUseCase, times(1)).execute(cpf);
        verify(findUserByCpf, times(1)).findByCpf(cpf);
    }
}
