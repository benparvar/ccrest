package com.benparvar.ccrest.usecase.update;

import com.benparvar.ccrest.domain.entity.UserEntityToUpdate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate user to update before transform to user use case test.
 */
class ValidateUserToUpdateBeforeTransformToUserUseCaseTest {
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final LocalDate VALID_LOCAL_DATE = LocalDate.now().minusYears(20L);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;
    private final Boolean VALID_ACTIVE_FALSE = Boolean.FALSE;

    private ValidateUserToUpdateBeforeTransformToUserUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateUserToUpdateBeforeTransformToUserUseCase();
    }

    /**
     * Execute with null user will fail.
     */
    @Test
    public void executeWithNullUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = null;

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("UserEntity cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty user will fail.
     */
    @Test
    public void executeWithEmptyUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder().build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null name user will fail.
     */
    @Test
    public void executeWithNullNameUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(null).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty name user will fail.
     */
    @Test
    public void executeWithEmptyNameUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name("").build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Name cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null surname user will fail.
     */
    @Test
    public void executeWithNullSurnameUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(null).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Surname cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty surname user will fail.
     */
    @Test
    public void executeWithEmptySurnameUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname("").build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Surname cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null birth date user will fail.
     */
    @Test
    public void executeWithNullBirthDateUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(null).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Birth date cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with in valid birth date user will fail.
     */
    @Test
    public void executeWithInValidBirthDateUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(LocalDate.now().plusDays(1L)).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Birth date must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null address user will fail.
     */
    @Test
    public void executeWithNullAddressUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(null).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Address cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null phones user will fail.
     */
    @Test
    public void executeWithNullPhonesUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(null).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Phones cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty phones user will fail.
     */
    @Test
    public void executeWithEmptyPhonesUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(Collections.emptyList()).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Phones cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null emails user will fail.
     */
    @Test
    public void executeWithNullEmailsUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(null).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Emails cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty emails user will fail.
     */
    @Test
    public void executeWithEmptyEmailsUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(Collections.emptyList()).build();
            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Emails cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null active user will fail.
     */
    @Test
    public void executeWithNullActiveUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(VALID_EMAILS)
                    .active(null).build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("Active cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with true active user will success.
     */
    @Test
    public void executeWithTrueActiveUserWillSuccess() {
        UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE).build();

        assertTrue(uc.execute(entity), "Must be a valid active user");
    }

    /**
     * Execute with false active user will success.
     */
    @Test
    public void executeWithFalseActiveUserWillSuccess() {
        UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_FALSE).build();

        assertTrue(uc.execute(entity), "Must be a valid inactive user");
    }
}