package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Separate inactive users use case test.
 */
class SeparateInactiveUsersUseCaseTest {
    private SeparateInactiveUsersUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new SeparateInactiveUsersUseCase();
    }

    /**
     * Execute with null user list will fail.
     */
    @Test
    public void executeWithNullUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = null;

            try {
                uc.execute(entities);
            } catch (IllegalArgumentException e) {
                assertEquals("User entity list cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty user list will success.
     */
    @Test
    public void executeWithAEmptyUserListWillSuccess() {
        List<UserEntity> entities = Collections.EMPTY_LIST;

        List<UserEntity> result = uc.execute(entities);

        assertNotNull(result, "List must be not null");
        assertTrue(result.isEmpty());
    }

    /**
     * Execute with a valid user list will success.
     */
    @Test
    public void executeWithAValidUserListWillSuccess() {
        UserEntity active1 = UserEntity.newBuilder().active(Boolean.TRUE).build();
        UserEntity active2 = UserEntity.newBuilder().active(Boolean.TRUE).build();

        UserEntity inactive1 = UserEntity.newBuilder().active(Boolean.FALSE).build();
        UserEntity inactive2 = UserEntity.newBuilder().active(Boolean.FALSE).build();
        UserEntity inactive3 = UserEntity.newBuilder().active(Boolean.FALSE).build();

        UserEntity nonRegistered1 = UserEntity.newBuilder().build();
        UserEntity nonRegistered2 = UserEntity.newBuilder().build();
        UserEntity nonRegistered3 = UserEntity.newBuilder().build();
        UserEntity nonRegistered4 = UserEntity.newBuilder().build();

        List<UserEntity> entities = Arrays.asList(active1, nonRegistered1, inactive1, nonRegistered2, active2, inactive2, nonRegistered3, inactive3, nonRegistered4);

        List<UserEntity> result = uc.execute(entities);

        assertNotNull(result, "List must be not null");
        assertEquals(3, result.size());
    }
}