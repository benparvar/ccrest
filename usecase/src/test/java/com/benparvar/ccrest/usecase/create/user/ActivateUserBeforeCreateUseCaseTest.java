package com.benparvar.ccrest.usecase.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Activate user before create use case test.
 */
public class ActivateUserBeforeCreateUseCaseTest {

    private ActivateUserBeforeCreateUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ActivateUserBeforeCreateUseCase();
    }

    /**
     * Execute with null user will fail.
     */
    @Test
    public void executeWithNullUserWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = null;

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("UserEntity cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty user will success.
     */
    @Test
    public void executeWithEmptyUserWillSuccess() {
        UserEntity entity = UserEntity.newBuilder().build();

        UserEntity result = uc.execute(entity);
        assertNotNull(result);
        assertTrue(result.getActive(), "User must be active");
    }
}
