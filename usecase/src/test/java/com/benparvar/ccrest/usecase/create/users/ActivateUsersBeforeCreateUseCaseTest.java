package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Activate users before create use case test.
 */
class ActivateUsersBeforeCreateUseCaseTest {
    private ActivateUsersBeforeCreateUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ActivateUsersBeforeCreateUseCase();
    }

    /**
     * Execute with null user list will fail.
     */
    @Test
    public void executeWithNullUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = null;

            try {
                uc.execute(entities);
            } catch (IllegalArgumentException e) {
                assertEquals("User entity list cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty user will return a empty list and success.
     */
    @Test
    public void executeWithEmptyUserWillReturnAEmptyListAndSuccess() {
        List<UserEntity> entities = Collections.emptyList();

        List<UserEntity> result = uc.execute(entities);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Execute with user list will success.
     */
    @Test
    public void executeWithUserListWillSuccess() {
        List<UserEntity> entities = Arrays.asList(UserEntity.newBuilder().cpf("0").build(),
                UserEntity.newBuilder().cpf("1").build());

        List<UserEntity> result = uc.execute(entities);
        assertNotNull(result);
        assertEquals(2, result.size());

        assertTrue(result.get(0).getActive(), "User must be active");
        assertEquals("0", result.get(0).getCpf(), "User cpf must be 0");

        assertTrue(result.get(1).getActive(), "User must be active");
        assertEquals("1", result.get(1).getCpf(), "User cpf must be 1");
    }
}