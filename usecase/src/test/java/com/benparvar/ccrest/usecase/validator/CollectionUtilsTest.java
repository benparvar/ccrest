package com.benparvar.ccrest.usecase.validator;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The type Collection utils test.
 */
public class CollectionUtilsTest {

    /**
     * Execute with a null collection will return true.
     */
    @Test
    public void executeWithANullCollectionWillReturnTrue() {
        List<String> collection = null;

        boolean result = CollectionUtils.isEmpty(collection);
        assertTrue(result);
    }

    /**
     * Execute with a empty collection will return true.
     */
    @Test
    public void executeWithAEmptyCollectionWillReturnTrue() {
        List<String> collection = new ArrayList<>();

        boolean result = CollectionUtils.isEmpty(collection);
        assertTrue(result);
    }

    /**
     * Execute with a non empty collection will return false.
     */
    @Test
    public void executeWithANonEmptyCollectionWillReturnFalse() {
        List<String> collection = Arrays.asList("Compulsion", "I had this thing");

        boolean result = CollectionUtils.isEmpty(collection);
        assertFalse(result);
    }
}
