package com.benparvar.ccrest.usecase.validator;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssertTest {

    @Test
    public void executeNotEmptyWithANullArrayWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                String[] array = null;
                Assert.notEmpty(array, "Cannot be null or empty");
            } catch (IllegalArgumentException e) {
                assertEquals("Cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");

        });
    }

    @Test
    public void executeNotEmptyWithANullCollectionWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                List<String> list = null;
                Assert.notEmpty(list, "Cannot be null or empty");
            } catch (IllegalArgumentException e) {
                assertEquals("Cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");

        });
    }
}
