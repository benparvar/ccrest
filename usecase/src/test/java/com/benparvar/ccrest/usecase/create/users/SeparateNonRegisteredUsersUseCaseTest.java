package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Separate non registered users use case test.
 */
class SeparateNonRegisteredUsersUseCaseTest {
    private SeparateNonRegisteredUsersUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new SeparateNonRegisteredUsersUseCase();
    }

    /**
     * Execute with null user list and null already registered user list will fail.
     */
    @Test
    public void executeWithNullUserListAndNullAlreadyRegisteredUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = null;
            List<UserEntity> alreadyRegisteredEntities = null;

            try {
                uc.execute(entities, alreadyRegisteredEntities);
            } catch (IllegalArgumentException e) {
                assertEquals("User entity list cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty user list will success.
     */
    @Test
    public void executeWithAEmptyUserListWillSuccess() {
        List<UserEntity> entities = Collections.EMPTY_LIST;
        List<UserEntity> alreadyRegisteredEntities = Collections.EMPTY_LIST;

        List<UserEntity> result = uc.execute(entities, alreadyRegisteredEntities);

        assertNotNull(result, "List must be not null");
        assertTrue(result.isEmpty());
    }

    /**
     * Execute with a valid user list will success.
     */
    @Test
    public void executeWithAValidUserListWillSuccess() {
        UserEntity active1 = UserEntity.newBuilder().cpf("263.569.080-41").active(Boolean.TRUE).build();
        UserEntity active2 = UserEntity.newBuilder().cpf("750.898.870-12").active(Boolean.TRUE).build();

        UserEntity inactive1 = UserEntity.newBuilder().cpf("398.970.920-80").active(Boolean.FALSE).build();
        UserEntity inactive2 = UserEntity.newBuilder().cpf("656.172.970-66").active(Boolean.FALSE).build();
        UserEntity inactive3 = UserEntity.newBuilder().cpf("560.977.990-12").active(Boolean.FALSE).build();

        UserEntity nonRegistered1 = UserEntity.newBuilder().cpf("163.893.930-67").build();
        UserEntity nonRegistered2 = UserEntity.newBuilder().cpf("850.611.550-76").build();
        UserEntity nonRegistered3 = UserEntity.newBuilder().cpf("983.918.720-17").build();
        UserEntity nonRegistered4 = UserEntity.newBuilder().cpf("861.086.960-10").build();

        List<UserEntity> entities = Arrays.asList(active1, nonRegistered1, inactive1, nonRegistered2, active2, inactive2, nonRegistered3, inactive3, nonRegistered4);
        List<UserEntity> alreadyRegisteredEntities = Arrays.asList(active1, inactive1, active2, inactive2, inactive3);

        List<UserEntity> result = uc.execute(entities, alreadyRegisteredEntities);

        assertNotNull(result, "List must be not null");
        assertEquals(4, result.size());
    }
}