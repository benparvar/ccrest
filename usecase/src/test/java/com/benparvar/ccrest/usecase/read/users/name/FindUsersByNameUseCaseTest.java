package com.benparvar.ccrest.usecase.read.users.name;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find users by name use case test.
 */
public class FindUsersByNameUseCaseTest {
    private final FindUsersByName findUsersByName = mock(FindUsersByName.class);
    private FindUsersByNameUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindUsersByNameUseCase(findUsersByName);
    }

    /**
     * Execute with a non existent name will fail.
     */
    @Test
    public void executeWithANonExistentNameWillFail() {
        assertThrows(UserNotFoundException.class, () -> {
            String name = "Xablau";

            when(findUsersByName.findByName(name)).thenReturn(Collections.emptyList());

            try {
                uc.execute(name);
            } catch (UserNotFoundException e) {
                verify(findUsersByName, times(1)).findByName(name);
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute with a existent name will success.
     */
    @Test
    public void executeWithAExistentNameWillSuccess() {
        String name = "Xemele";

        when(findUsersByName.findByName(name)).thenReturn(Arrays.asList(UserEntity.newBuilder().name(name).build()));

        List<UserEntity> entities = uc.execute(name);
        assertNotNull(entities);
        assertEquals(1, entities.size());
        assertEquals(name, entities.get(0).getName());

        verify(findUsersByName, times(1)).findByName(name);

    }
}
