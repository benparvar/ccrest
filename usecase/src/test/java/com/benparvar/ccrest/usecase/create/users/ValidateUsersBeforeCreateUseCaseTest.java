package com.benparvar.ccrest.usecase.create.users;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate users before create use case test.
 */
class ValidateUsersBeforeCreateUseCaseTest {

    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final LocalDate VALID_LOCAL_DATE = LocalDate.now().minusYears(20L);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;
    private final Boolean VALID_ACTIVE_FALSE = Boolean.FALSE;

    private ValidateUsersBeforeCreateUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateUsersBeforeCreateUseCase();
    }

    /**
     * Execute with null user list will fail.
     */
    @Test
    public void executeWithNullUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = null;

            try {
                uc.execute(entities);
            } catch (IllegalArgumentException e) {
                assertEquals("UserEntity list cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty user list will fail.
     */
    @Test
    public void executeWithEmptyUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = Collections.EMPTY_LIST;

            try {
                uc.execute(entities);
            } catch (IllegalArgumentException e) {
                assertEquals("UserEntity list cannot be null or empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with user list with null users will fail.
     */
    @Test
    public void executeWithUserListWithNullUsersWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = null;

            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any null user", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null cpf user list will fail.
     */
    @Test
    public void executeWithNullCPFUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder().cpf(null).build();

            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null CPF", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty cpf user list will fail.
     */
    @Test
    public void executeWithEmptyCPFUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder().cpf("").build();

            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null CPF", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with invalid alpha numeric cpf user list will fail.
     */
    @Test
    public void executeWithInvalidAlphaNumericCPFUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder().cpf("abc123").build();

            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("Every CPF on the list must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with invalid alpha cpf user list will fail.
     */
    @Test
    public void executeWithInvalidAlphaCPFUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder().cpf("abcasd").build();

            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("Every CPF on the list must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with invalid numeric cpf user list will fail.
     */
    @Test
    public void executeWithInvalidNumericCPFUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder().cpf("11143211111").build();

            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("Every CPF on the list must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null name user list will fail.
     */
    @Test
    public void executeWithNullNameUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(null).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null name", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty name user list will fail.
     */
    @Test
    public void executeWithEmptyNameUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name("").build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null name", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null surname user list will fail.
     */
    @Test
    public void executeWithNullSurnameUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(null).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null surname", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty surname user list will fail.
     */
    @Test
    public void executeWithEmptySurnameUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname("").build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null surname", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null birth date user list will fail.
     */
    @Test
    public void executeWithNullBirthDateUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(null).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null birth date", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with in valid birth date user list will fail.
     */
    @Test
    public void executeWithInValidBirthDateUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(LocalDate.now().plusDays(1L)).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any invalid birth date", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null address user list will fail.
     */
    @Test
    public void executeWithNullAddressUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(null).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null address", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty address user list will fail.
     */
    @Test
    public void executeWithEmptyAddressUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address("").build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null address", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null phones user list will fail.
     */
    @Test
    public void executeWithNullPhonesUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(null).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null phone list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null phone in the list user list will fail.
     */
    @Test
    public void executeWithNullPhoneInTheListUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<String> phones = Arrays.asList("(11) 9123-1231", null, "(15) 8888-1231");

            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(phones).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null phone in the list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty phone in the list user list will fail.
     */
    @Test
    public void executeWithEmptyPhoneInTheListUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<String> phones = Arrays.asList("(11) 9123-1231", "", "(15) 8888-1231");

            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(phones).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null phone in the list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty phones user list will fail.
     */
    @Test
    public void executeWithEmptyPhonesUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(Collections.emptyList()).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null phone list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null emails user list will fail.
     */
    @Test
    public void executeWithNullEmailsUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(null).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null email list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty emails user list will fail.
     */
    @Test
    public void executeWithEmptyEmailsUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(Collections.emptyList()).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null email list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null email in the list user list will fail.
     */
    @Test
    public void executeWithNullEmailInTheListUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<String> emails = Arrays.asList("jurubeba@gmail.com", "blz@gmail.com", "fr@frc.gov", null, "darcy@gmail.com");
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(emails).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null email in the list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty email in the list user list will fail.
     */
    @Test
    public void executeWithEmptyEmailInTheListUserListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<String> emails = Arrays.asList("jurubeba@gmail.com", "blz@gmail.com", "fr@frc.gov", "", "darcy@gmail.com");
            UserEntity entity = UserEntity.newBuilder()
                    .cpf(VALID_CPF)
                    .name(VALID_NAME)
                    .surname(VALID_SURNAME)
                    .birthDate(VALID_LOCAL_DATE)
                    .address(VALID_ADDRESS)
                    .phones(VALID_PHONES)
                    .emails(emails).build();
            try {
                uc.execute(Arrays.asList(entity));
            } catch (IllegalArgumentException e) {
                assertEquals("List cannot have any empty or null email in the list", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with null active user list will success.
     */
    @Test
    public void executeWithNullActiveUserListWillSuccess() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(null).build();

        assertTrue(uc.execute(Arrays.asList(entity)), "All users in the list must be valid");
    }

    /**
     * Execute with true active user list will success.
     */
    @Test
    public void executeWithTrueActiveUserListWillSuccess() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE).build();

        assertTrue(uc.execute(Arrays.asList(entity)), "All users in the list must be valid");
    }

    /**
     * Execute with false active user list will success.
     */
    @Test
    public void executeWithFalseActiveUserListWillSuccess() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_FALSE).build();

        assertTrue(uc.execute(Arrays.asList(entity)), "All users in the list must be valid.");
    }
}