package com.benparvar.ccrest.controller.update.user;

import com.benparvar.ccrest.controller.model.update.user.UpdateUserRequest;
import com.benparvar.ccrest.controller.model.update.user.UpdateUserRequestMapper;
import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.domain.entity.UserEntityToUpdate;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The type Update user request mapper test.
 */
class UpdateUserRequestMapperTest {
    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final String VALID_LOCAL_DATE = "2019-12-31";
    private final LocalDate VALID_ENTITY_LOCAL_DATE = LocalDate.of(2019, 12, 31);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    /**
     * Should map empty entity to request.
     */
    @Test
    public void shouldMapEmptyEntityToRequest() {
        UserEntity entity = UserEntity.newBuilder().build();
        UpdateUserRequest request = UpdateUserRequestMapper.INSTANCE.entityToRequest(entity);

        assertNotNull(request);
    }

    /**
     * Should map entity to request.
     */
    @Test
    public void shouldMapEntityToRequest() {
        UserEntity entity = UserEntity.newBuilder()
                .id(VALID_ID)
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();
        UpdateUserRequest request = UpdateUserRequestMapper.INSTANCE.entityToRequest(entity);

        assertNotNull(request);

        assertTrue(entity.getName().equals(request.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((request.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    /**
     * Should map empty request to entity.
     */
    @Test
    public void shouldMapEmptyRequestToEntity() {
        UpdateUserRequest request = new UpdateUserRequest();
        UserEntity entity = UpdateUserRequestMapper.INSTANCE.requestToEntity(request);

        assertNotNull(entity);
    }

    /**
     * Should map request to entity.
     */
    @Test
    public void shouldMapRequestToEntity() {
        UpdateUserRequest request = new UpdateUserRequest();

        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        UserEntity entity = UpdateUserRequestMapper.INSTANCE.requestToEntity(request);

        assertNotNull(entity);
        assertTrue(entity.getName().equals(request.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((request.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    /**
     * Should map empty entity to update to request.
     */
    @Test
    public void shouldMapEmptyEntityToUpdateToRequest() {
        UserEntityToUpdate entity = UserEntityToUpdate.newBuilder().build();
        UpdateUserRequest request = UpdateUserRequestMapper.INSTANCE.entityToUpdateToRequest(entity);

        assertNotNull(request);
    }

    /**
     * Should map entity to update to request.
     */
    @Test
    public void shouldMapEntityToUpdateToRequest() {
        UserEntityToUpdate entity = UserEntityToUpdate.newBuilder()
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();
        UpdateUserRequest request = UpdateUserRequestMapper.INSTANCE.entityToUpdateToRequest(entity);

        assertNotNull(request);

        assertTrue(entity.getName().equals(request.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((request.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    /**
     * Should map empty request to entity to update.
     */
    @Test
    public void shouldMapEmptyRequestToEntityToUpdate() {
        UpdateUserRequest request = new UpdateUserRequest();
        UserEntityToUpdate entity = UpdateUserRequestMapper.INSTANCE.requestToEntityToUpdate(request);

        assertNotNull(entity);
    }

    /**
     * Should map request to entity to update.
     */
    @Test
    public void shouldMapRequestToEntityToUpdate() {
        UpdateUserRequest request = new UpdateUserRequest();

        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        UserEntityToUpdate entity = UpdateUserRequestMapper.INSTANCE.requestToEntityToUpdate(request);

        assertNotNull(entity);
        assertTrue(entity.getName().equals(request.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((request.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(request.getEmails()), "Emails must be equal");
    }
}