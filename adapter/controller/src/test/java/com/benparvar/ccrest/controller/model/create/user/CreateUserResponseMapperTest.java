package com.benparvar.ccrest.controller.model.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Create user response mapper test.
 */
public class CreateUserResponseMapperTest {
    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final String VALID_LOCAL_DATE = "2019-12-31";
    private final LocalDate VALID_ENTITY_LOCAL_DATE = LocalDate.of(2019, 12, 31);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    /**
     * Should map empty entity to response.
     */
    @Test
    public void shouldMapEmptyEntityToResponse() {
        UserEntity entity = UserEntity.newBuilder().build();
        CreateUserResponse response = CreateUserResponseMapper.INSTANCE.entityToResponse(entity);

        assertNotNull(response);
    }

    /**
     * Should map entity to response.
     */
    @Test
    public void shouldMapEntityToResponse() {
        UserEntity entity = UserEntity.newBuilder()
                .id(VALID_ID)
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();
        CreateUserResponse response = CreateUserResponseMapper.INSTANCE.entityToResponse(entity);

        assertNotNull(response);
        assertTrue(entity.getCpf().equals(response.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(response.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(response.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(response.getBirthDate()), "Birth date must be equal");
        assertTrue((response.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(response.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(response.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(response.getEmails()), "Emails must be equal");
    }

    /**
     * Should map empty response to entity.
     */
    @Test
    public void shouldMapEmptyResponseToEntity() {
        CreateUserResponse response = new CreateUserResponse();
        UserEntity entity = CreateUserResponseMapper.INSTANCE.responseToEntity(response);

        assertNotNull(entity);
    }

    /**
     * Should map response to entity.
     */
    @Test
    public void shouldMapResponseToEntity() {
        CreateUserResponse response = new CreateUserResponse();

        response.setCpf(VALID_CPF);
        response.setName(VALID_NAME);
        response.setSurname(VALID_SURNAME);
        response.setBirthDate(VALID_LOCAL_DATE);
        response.setAddress(VALID_ADDRESS);
        response.setPhones(VALID_PHONES);
        response.setEmails(VALID_EMAILS);

        UserEntity entity = CreateUserResponseMapper.INSTANCE.responseToEntity(response);

        assertNotNull(entity);
        assertTrue(entity.getCpf().equals(response.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(response.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(response.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(response.getBirthDate()), "Birth date must be equal");
        assertTrue((response.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(response.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(response.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(response.getEmails()), "Emails must be equal");
    }


    /**
     * Should map empty entity list to response.
     */
    @Test
    public void shouldMapEmptyEntityListToResponse() {
        List<CreateUserResponse> responses = CreateUserResponseMapper.INSTANCE.entitiesToResponse(Collections.emptyList());

        assertNotNull(responses);
        assertTrue(responses.isEmpty());
    }


    /**
     * Should map entity list to response.
     */
    @Test
    public void shouldMapEntityListToResponse() {
        UserEntity entity = UserEntity.newBuilder()
                .id(VALID_ID)
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();
        List<CreateUserResponse> responses = CreateUserResponseMapper.INSTANCE.entitiesToResponse(Arrays.asList(entity));

        assertNotNull(responses);
        assertFalse(responses.isEmpty());
        CreateUserResponse response = responses.get(0);

        assertTrue(entity.getCpf().equals(response.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(response.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(response.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(response.getBirthDate()), "Birth date must be equal");
        assertTrue((response.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(response.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(response.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(response.getEmails()), "Emails must be equal");
    }
}