package com.benparvar.ccrest.controller.model.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Create user request mapper test.
 */
public class CreateUserRequestMapperTest {
    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final String VALID_LOCAL_DATE = "2019-12-31";
    private final LocalDate VALID_ENTITY_LOCAL_DATE = LocalDate.of(2019, 12, 31);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    /**
     * Should map empty entity to request.
     */
    @Test
    public void shouldMapEmptyEntityToRequest() {
        UserEntity entity = UserEntity.newBuilder().build();
        CreateUserRequest request = CreateUserRequestMapper.INSTANCE.entityToRequest(entity);

        assertNotNull(request);
    }

    /**
     * Should map entity to request.
     */
    @Test
    public void shouldMapEntityToRequest() {
        UserEntity entity = UserEntity.newBuilder()
                .id(VALID_ID)
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();
        CreateUserRequest request = CreateUserRequestMapper.INSTANCE.entityToRequest(entity);

        assertNotNull(request);
        assertTrue(entity.getCpf().equals(request.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(request.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((request.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    /**
     * Should map empty request to entity.
     */
    @Test
    public void shouldMapEmptyRequestToEntity() {
        CreateUserRequest request = new CreateUserRequest();
        UserEntity entity = CreateUserRequestMapper.INSTANCE.requestToEntity(request);

        assertNotNull(entity);
    }

    /**
     * Should map request to entity.
     */
    @Test
    public void shouldMapRequestToEntity() {
        CreateUserRequest request = new CreateUserRequest();

        request.setCpf(VALID_CPF);
        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        UserEntity entity = CreateUserRequestMapper.INSTANCE.requestToEntity(request);

        assertNotNull(entity);
        assertTrue(entity.getCpf().equals(request.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(request.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((request.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(entity.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    /**
     * Should map a null list of request to entity.
     */
    @Test
    public void shouldMapANullListOfRequestToEntity() {
        List<CreateUserRequest> request = null;
        List<UserEntity> entities = CreateUserRequestMapper.INSTANCE.requestToEntities(request);

        assertNull(entities);
    }

    /**
     * Should map list of empty request to entity.
     */
    @Test
    public void shouldMapListOfEmptyRequestToEntity() {
        List<CreateUserRequest> request = Collections.emptyList();
        List<UserEntity> entities = CreateUserRequestMapper.INSTANCE.requestToEntities(request);

        assertNotNull(entities);
        assertTrue(entities.isEmpty(), "UserEntity list must be empty");
    }

    /**
     * Should map list request to list entity.
     */
    @Test
    public void shouldMapListRequestToListEntity() {
        List<CreateUserRequest> request = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            CreateUserRequest createUserRequest = new CreateUserRequest();
            createUserRequest.setCpf(VALID_CPF);
            createUserRequest.setName(VALID_NAME);
            createUserRequest.setSurname(VALID_SURNAME);
            createUserRequest.setBirthDate(VALID_LOCAL_DATE);
            createUserRequest.setAddress(VALID_ADDRESS);
            createUserRequest.setPhones(VALID_PHONES);
            createUserRequest.setEmails(VALID_EMAILS);

            request.add(createUserRequest);
        }

        List<UserEntity> entities = CreateUserRequestMapper.INSTANCE.requestToEntities(request);

        assertNotNull(entities);
        assertTrue(entities.size() == 10, "UserEntity list must have 10 items");

        assertTrue(entities.get(0).getCpf().equals(VALID_CPF), "CPF must be equal");
        assertTrue(entities.get(0).getName().equals(VALID_NAME), "Name must be equal");
        assertTrue(entities.get(0).getSurname().equals(VALID_SURNAME), "Surname must be equal");
        assertTrue(entities.get(0).getBirthDate().toString().equals(VALID_LOCAL_DATE), "Birth date must be equal");
        assertTrue(entities.get(0).getAddress().equals(VALID_ADDRESS), "Address must be equal");
        assertTrue(entities.get(0).getPhones().equals(VALID_PHONES), "Phones must be equal");
        assertTrue(entities.get(0).getEmails().equals(VALID_EMAILS), "Emails must be equal");
    }
}