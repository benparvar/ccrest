package com.benparvar.ccrest.controller;

import com.benparvar.ccrest.controller.model.create.user.CreateUserRequest;
import com.benparvar.ccrest.controller.model.create.user.CreateUserRequestMapper;
import com.benparvar.ccrest.controller.model.create.user.CreateUserResponse;
import com.benparvar.ccrest.controller.model.read.user.ReadUserResponse;
import com.benparvar.ccrest.controller.model.update.user.UpdateUserRequest;
import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.create.user.CreateUserUseCase;
import com.benparvar.ccrest.usecase.create.users.CreateUsersUseCase;
import com.benparvar.ccrest.usecase.delete.DeleteUserUseCase;
import com.benparvar.ccrest.usecase.exception.UserAlreadyExistsException;
import com.benparvar.ccrest.usecase.exception.UserNotFoundException;
import com.benparvar.ccrest.usecase.read.user.cpf.FindUserByCpfUseCase;
import com.benparvar.ccrest.usecase.read.users.name.FindUsersByNameUseCase;
import com.benparvar.ccrest.usecase.read.users.surname.FindUsersBySurnameUseCase;
import com.benparvar.ccrest.usecase.update.UpdateUserUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * The type User controller test.
 */
public class UserControllerTest {
    private final CreateUserUseCase createUserUseCase = mock(CreateUserUseCase.class);
    private final CreateUsersUseCase createUsersUseCase = mock(CreateUsersUseCase.class);
    private final DeleteUserUseCase deleteUserUseCase = mock(DeleteUserUseCase.class);
    private final UpdateUserUseCase updateUserUseCase = mock(UpdateUserUseCase.class);
    private final FindUserByCpfUseCase findUserByCpfUseCase = mock(FindUserByCpfUseCase.class);
    private final FindUsersByNameUseCase findUsersByNameUseCase = mock(FindUsersByNameUseCase.class);
    private final FindUsersBySurnameUseCase findUsersBySurnameUseCase = mock(FindUsersBySurnameUseCase.class);

    private UserController controller;

    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final String VALID_LOCAL_DATE = "2019-12-31";
    private final LocalDate VALID_ENTITY_LOCAL_DATE = LocalDate.of(2019, 12, 31);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        controller = new UserController(createUserUseCase, createUsersUseCase, deleteUserUseCase, updateUserUseCase,
                findUserByCpfUseCase, findUsersByNameUseCase, findUsersBySurnameUseCase);
    }

    // createUser

    /**
     * Execute create method with a null user will fail.
     */
    @Test
    public void executeCreateMethodWithANullUserWillFail() {
        CreateUserRequest request = null;
        when(createUserUseCase.execute(any())).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                CreateUserResponse response = controller.create(request);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute create method with a valid user will success.
     */
    @Test
    public void executeCreateMethodWithAValidUserWillSuccess() {
        CreateUserRequest request = new CreateUserRequest();
        request.setCpf(VALID_CPF);
        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        UserEntity entity = CreateUserRequestMapper.INSTANCE.requestToEntity(request);

        when(createUserUseCase.execute(any())).thenReturn(entity);

        CreateUserResponse response = controller.create(request);

        assertNotNull(response);
        assertTrue(response.getCpf().equals(request.getCpf()), "CPF must be equal");
        assertTrue(response.getName().equals(request.getName()), "Name must be equal");
        assertTrue(response.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(response.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((response.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(response.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(response.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(response.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    /**
     * Execute create method with a valid pre existent user will fail.
     */
    @Test
    public void executeCreateMethodWithAValidPreExistentUserWillFail() {
        CreateUserRequest request = new CreateUserRequest();
        request.setCpf(VALID_CPF);
        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        when(createUserUseCase.execute(any())).thenThrow(new UserAlreadyExistsException());

        assertThrows(UserAlreadyExistsException.class, () -> {
            try {
                CreateUserResponse response = controller.create(request);
            } catch (UserAlreadyExistsException e) {
                throw e;
            }

            fail("should throw an UserAlreadyExistsException");
        });
    }

    // createUsers

    /**
     * Execute bulk create method with a null user list will fail.
     */
    @Test
    public void executeBulkCreateMethodWithANullUserListWillFail() {
        List<CreateUserRequest> request = null;
        when(createUsersUseCase.execute(any())).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                List<CreateUserResponse> response = controller.bulkCreate(request);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute bulk create method with a valid user list will success.
     */
    @Test
    public void executeBulkCreateMethodWithAValidUserListWillSuccess() {
        CreateUserRequest request = new CreateUserRequest();
        request.setCpf(VALID_CPF);
        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        List<UserEntity> entities = CreateUserRequestMapper.INSTANCE.requestToEntities(Arrays.asList(request));

        when(createUsersUseCase.execute(any())).thenReturn(entities);

        List<CreateUserResponse> responses = controller.bulkCreate(Arrays.asList(request));

        assertNotNull(responses);
        assertFalse(responses.isEmpty());

        CreateUserResponse response = responses.get(0);

        assertTrue(response.getCpf().equals(request.getCpf()), "CPF must be equal");
        assertTrue(response.getName().equals(request.getName()), "Name must be equal");
        assertTrue(response.getSurname().equals(request.getSurname()), "Surname must be equal");
        assertTrue(response.getBirthDate().toString().equals(request.getBirthDate()), "Birth date must be equal");
        assertTrue((response.getBirthDate().equals(VALID_LOCAL_DATE)), "Birth date must be in a valid format (yyyy-MM-dd)");
        assertTrue(response.getAddress().equals(request.getAddress()), "Address must be equal");
        assertTrue(response.getPhones().equals(request.getPhones()), "Phones must be equal");
        assertTrue(response.getEmails().equals(request.getEmails()), "Emails must be equal");
    }

    // deleteUser

    /**
     * Execute delete method with a null cpf will fail.
     */
    @Test
    public void executeDeleteMethodWithANullCpfWillFail() {
        String cpf = null;
        when(deleteUserUseCase.execute(any())).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                controller.delete(cpf);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute delete method with a non existent cpf will fail.
     */
    @Test
    public void executeDeleteMethodWithANonExistentCpfWillFail() {
        String cpf = VALID_CPF;
        when(deleteUserUseCase.execute(cpf)).thenThrow(new UserNotFoundException());

        assertThrows(UserNotFoundException.class, () -> {
            try {
                controller.delete(cpf);
            } catch (UserNotFoundException e) {
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute delete method with existent cpf will success.
     */
    @Test
    public void executeDeleteMethodWithExistentCpfWillSuccess() {
        String cpf = VALID_CPF;
        when(deleteUserUseCase.execute(cpf)).thenReturn(1);
        controller.delete(cpf);

        verify(deleteUserUseCase, times(1)).execute(cpf);
    }

    // updateUser

    /**
     * Execute update method with a null user will fail.
     */
    @Test
    public void executeUpdateMethodWithANullUserWillFail() {
        UpdateUserRequest request = null;
        String cpf = VALID_CPF;
        when(updateUserUseCase.execute(any(), eq(cpf))).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                controller.update(request, cpf);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute update method with a valid user and cpf will success.
     */
    @Test
    public void executeUpdateMethodWithAValidUserAndCpfWillSuccess() {
        UpdateUserRequest request = new UpdateUserRequest();
        request.setName(VALID_NAME);
        request.setSurname(VALID_SURNAME);
        request.setBirthDate(VALID_LOCAL_DATE);
        request.setAddress(VALID_ADDRESS);
        request.setPhones(VALID_PHONES);
        request.setEmails(VALID_EMAILS);

        String cpf = VALID_CPF;

        when(updateUserUseCase.execute(any(), eq(cpf))).thenReturn(1);

        controller.update(request, cpf);

        verify(updateUserUseCase, times(1)).execute(any(), eq(cpf));
    }

    // findUserByCpf

    /**
     * Execute find by cpf method with a null cpf will fail.
     */
    @Test
    public void executeFindByCpfMethodWithANullCpfWillFail() {
        String cpf = null;
        when(findUserByCpfUseCase.execute(any())).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                ReadUserResponse response = controller.findByCpf(cpf);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute find by cpf method with a non existent cpf will fail.
     */
    @Test
    public void executeFindByCpfMethodWithANonExistentCpfWillFail() {
        String cpf = VALID_CPF;
        when(findUserByCpfUseCase.execute(cpf)).thenThrow(new UserNotFoundException());

        assertThrows(UserNotFoundException.class, () -> {
            try {
                ReadUserResponse response = controller.findByCpf(cpf);
            } catch (UserNotFoundException e) {
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute find by cpf method with a valid pre existent cpf will success.
     */
    @Test
    public void executeFindByCpfMethodWithAValidPreExistentCpfWillSuccess() {
        String cpf = VALID_CPF;

        UserEntity entity = UserEntity.newBuilder().cpf(VALID_CPF).build();

        when(findUserByCpfUseCase.execute(cpf)).thenReturn(entity);

        ReadUserResponse response = controller.findByCpf(cpf);

        assertNotNull(response);
        assertEquals(cpf, response.getCpf());
    }

    // findUsersByName

    /**
     * Execute find by name method with a null name will fail.
     */
    @Test
    public void executeFindByNameMethodWithANullNameWillFail() {
        String name = null;
        when(findUsersByNameUseCase.execute(any())).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                List<ReadUserResponse> responses = controller.findByName(name);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute find by name method with a non existent name will fail.
     */
    @Test
    public void executeFindByNameMethodWithANonExistentNameWillFail() {
        String name = VALID_NAME;
        when(findUsersByNameUseCase.execute(any())).thenThrow(new UserNotFoundException());

        assertThrows(UserNotFoundException.class, () -> {
            try {
                List<ReadUserResponse> responses = controller.findByName(name);
            } catch (UserNotFoundException e) {
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute find by name method with a valid pre existent name will success.
     */
    @Test
    public void executeFindByNameMethodWithAValidPreExistentNameWillSuccess() {
        String name = VALID_NAME;

        UserEntity entity = UserEntity.newBuilder().name(VALID_NAME).build();

        when(findUsersByNameUseCase.execute(name)).thenReturn(Arrays.asList(entity));

        List<ReadUserResponse> responses = controller.findByName(name);

        assertNotNull(responses);
        assertFalse(responses.isEmpty());

        ReadUserResponse response = responses.get(0);
        assertEquals(name, response.getName());
    }

    // findUsersBySurname

    /**
     * Execute find by surname method with a null surname will fail.
     */
    @Test
    public void executeFindBySurnameMethodWithANullSurnameWillFail() {
        String surname = null;
        when(findUsersBySurnameUseCase.execute(surname)).thenThrow(new IllegalArgumentException());

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                List<ReadUserResponse> responses = controller.findBySurname(surname);
            } catch (IllegalArgumentException e) {
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute find by surname method with a non existent surname will fail.
     */
    @Test
    public void executeFindBySurnameMethodWithANonExistentSurnameWillFail() {
        String surname = VALID_SURNAME;
        when(findUsersBySurnameUseCase.execute(surname)).thenThrow(new UserNotFoundException());

        assertThrows(UserNotFoundException.class, () -> {
            try {
                List<ReadUserResponse> responses = controller.findBySurname(surname);
            } catch (UserNotFoundException e) {
                throw e;
            }

            fail("should throw an UserNotFoundException");
        });
    }

    /**
     * Execute find by surname method with a valid pre existent surname will success.
     */
    @Test
    public void executeFindBySurnameMethodWithAValidPreExistentSurnameWillSuccess() {
        String surname = VALID_SURNAME;

        UserEntity entity = UserEntity.newBuilder().surname(VALID_SURNAME).build();

        when(findUsersBySurnameUseCase.execute(surname)).thenReturn(Arrays.asList(entity));

        List<ReadUserResponse> responses = controller.findBySurname(surname);

        assertNotNull(responses);
        assertFalse(responses.isEmpty());

        ReadUserResponse response = responses.get(0);
        assertEquals(surname, response.getSurname());
    }
}
