package com.benparvar.ccrest.controller.update.user;

import com.benparvar.ccrest.controller.model.update.user.UpdateUserResponse;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type Update user response test.
 */
class UpdateUserResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateUserResponse obj1 = new UpdateUserResponse();

        obj1.setCpf("336.519.850-40");
        obj1.setName("Hashy");
        obj1.setSurname("Ilusion");
        obj1.setBirthDate("12-31-2000");
        obj1.setAddress("Desulional St, 21");
        obj1.setEmails(Arrays.asList("del@ded.gov"));
        obj1.setPhones(Arrays.asList("(11) 8888-1231"));
        obj1.setActive(Boolean.TRUE);

        UpdateUserResponse obj2 = new UpdateUserResponse();

        obj2.setCpf("336.519.850-40");
        obj2.setName("Hashy");
        obj2.setSurname("Ilusion");
        obj2.setBirthDate("12-31-2000");
        obj2.setAddress("Desulional St, 21");
        obj2.setEmails(Arrays.asList("del@ded.gov"));
        obj2.setPhones(Arrays.asList("(11) 8888-1231"));
        obj2.setActive(Boolean.TRUE);

        assertEquals(obj1.hashCode(), obj2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateUserResponse obj1 = new UpdateUserResponse();

        obj1.setCpf("336.519.850-40");
        obj1.setName("Hashy");
        obj1.setSurname("Ilusion");
        obj1.setBirthDate("12-31-2000");
        obj1.setAddress("Desulional St, 21");
        obj1.setEmails(Arrays.asList("del@ded.gov"));
        obj1.setPhones(Arrays.asList("(11) 8888-1231"));
        obj1.setActive(Boolean.TRUE);

        UpdateUserResponse obj2 = new UpdateUserResponse();

        obj2.setCpf("336.519.850-40");
        obj2.setName("Hashy");
        obj2.setSurname("Ilusion");
        obj2.setBirthDate("12-31-2000");
        obj2.setAddress("Desulional St, 21");
        obj2.setEmails(Arrays.asList("del@ded.gov"));
        obj2.setPhones(Arrays.asList("(11) 8888-1231"));
        obj2.setActive(Boolean.TRUE);

        assertEquals(obj1, obj2);
    }
}