module com.benparvar.ccrest.controller {
    exports com.benparvar.ccrest.controller;
    exports com.benparvar.ccrest.controller.model.update.user;
    exports com.benparvar.ccrest.controller.model.read.user;
    exports com.benparvar.ccrest.controller.model.create.user;

    requires com.benparvar.ccrest.usecase;
    requires com.benparvar.ccrest.domain;
    requires org.slf4j;
    requires org.mapstruct;
    requires java.sql;
}
