package com.benparvar.ccrest.controller.model.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;


/**
 * The interface Create user response mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CreateUserResponseMapper {
    /**
     * The constant INSTANCE.
     */
    CreateUserResponseMapper INSTANCE = Mappers.getMapper(CreateUserResponseMapper.class);

    /**
     * Entity to response create user response.
     *
     * @param entity the entity
     * @return the create user response
     */
    CreateUserResponse entityToResponse(UserEntity entity);

    /**
     * Response to entity user entity.
     *
     * @param response the response
     * @return the user entity
     */
    UserEntity responseToEntity(CreateUserResponse response);

    /**
     * Entities to response list.
     *
     * @param entities the entities
     * @return the list
     */
    List<CreateUserResponse> entitiesToResponse(List<UserEntity> entities);
}
