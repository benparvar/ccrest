package com.benparvar.ccrest.controller.model.update.user;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * The type Update user request.
 */
public class UpdateUserRequest implements Serializable {
    private static final long serialVersionUID = 3275964415410776449L;
    private String name;
    private String surname;
    private String birthDate;
    private String address;
    private List<String> phones;
    private List<String> emails;
    private Boolean active;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phones.
     *
     * @return the phones
     */
    public List<String> getPhones() {
        return phones;
    }

    /**
     * Sets phones.
     *
     * @param phones the phones
     */
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    /**
     * Gets emails.
     *
     * @return the emails
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * Sets emails.
     *
     * @param emails the emails
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param active the active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateUserRequest that = (UpdateUserRequest) o;
        return name.equals(that.name) &&
                surname.equals(that.surname) &&
                birthDate.equals(that.birthDate) &&
                address.equals(that.address) &&
                phones.equals(that.phones) &&
                emails.equals(that.emails) &&
                active.equals(that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, address, phones, emails, active);
    }

    @Override
    public String toString() {
        return "UpdateUserRequest{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", address='" + address + '\'' +
                ", phones=" + phones +
                ", emails=" + emails +
                ", active=" + active +
                '}';
    }
}
