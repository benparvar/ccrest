package com.benparvar.ccrest.controller;

import com.benparvar.ccrest.controller.model.create.user.CreateUserRequest;
import com.benparvar.ccrest.controller.model.create.user.CreateUserRequestMapper;
import com.benparvar.ccrest.controller.model.create.user.CreateUserResponse;
import com.benparvar.ccrest.controller.model.create.user.CreateUserResponseMapper;
import com.benparvar.ccrest.controller.model.read.user.ReadUserResponse;
import com.benparvar.ccrest.controller.model.read.user.ReadUserResponseMapper;
import com.benparvar.ccrest.controller.model.update.user.UpdateUserRequest;
import com.benparvar.ccrest.controller.model.update.user.UpdateUserRequestMapper;
import com.benparvar.ccrest.usecase.create.user.CreateUserUseCase;
import com.benparvar.ccrest.usecase.create.users.CreateUsersUseCase;
import com.benparvar.ccrest.usecase.delete.DeleteUserUseCase;
import com.benparvar.ccrest.usecase.read.user.cpf.FindUserByCpfUseCase;
import com.benparvar.ccrest.usecase.read.users.name.FindUsersByNameUseCase;
import com.benparvar.ccrest.usecase.read.users.surname.FindUsersBySurnameUseCase;
import com.benparvar.ccrest.usecase.update.UpdateUserUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type User controller.
 */
public class UserController {
    private final Logger log = LoggerFactory.getLogger(UserController.class);
    private final CreateUserUseCase createUserUseCase;
    private final CreateUsersUseCase createUsersUseCase;
    private final DeleteUserUseCase deleteUserUseCase;
    private final UpdateUserUseCase updateUserUseCase;
    private final FindUserByCpfUseCase findUserByCpfUseCase;
    private final FindUsersByNameUseCase findUsersByNameUseCase;
    private final FindUsersBySurnameUseCase findUsersBySurnameUseCase;

    /**
     * Instantiates a new User controller.
     *
     * @param createUserUseCase         the create user use case
     * @param createUsersUseCase        the create users use case
     * @param deleteUserUseCase         the delete user use case
     * @param updateUserUseCase         the update user use case
     * @param findUserByCpfUseCase      the find user by cpf use case
     * @param findUsersByNameUseCase    the find users by name use case
     * @param findUsersBySurnameUseCase the find users by surname use case
     */
    public UserController(CreateUserUseCase createUserUseCase, CreateUsersUseCase createUsersUseCase,
                          DeleteUserUseCase deleteUserUseCase, UpdateUserUseCase updateUserUseCase,
                          FindUserByCpfUseCase findUserByCpfUseCase, FindUsersByNameUseCase findUsersByNameUseCase,
                          FindUsersBySurnameUseCase findUsersBySurnameUseCase) {
        this.createUserUseCase = createUserUseCase;
        this.createUsersUseCase = createUsersUseCase;
        this.deleteUserUseCase = deleteUserUseCase;
        this.updateUserUseCase = updateUserUseCase;
        this.findUserByCpfUseCase = findUserByCpfUseCase;
        this.findUsersByNameUseCase = findUsersByNameUseCase;
        this.findUsersBySurnameUseCase = findUsersBySurnameUseCase;
    }

    /**
     * Create create user response.
     *
     * @param request the request
     * @return the create user response
     */
    public CreateUserResponse create(CreateUserRequest request) {
        log.info("create: {}", request);

        return CreateUserResponseMapper.INSTANCE
                .entityToResponse(createUserUseCase.execute(CreateUserRequestMapper.INSTANCE.requestToEntity(request)));
    }

    /**
     * Bulk create list.
     *
     * @param request the request
     * @return the list
     */
    public List<CreateUserResponse> bulkCreate(List<CreateUserRequest> request) {
        log.info("bulkCreate: {}", request);

        return CreateUserResponseMapper.INSTANCE
                .entitiesToResponse(createUsersUseCase.execute(CreateUserRequestMapper.INSTANCE
                        .requestToEntities(request)));
    }

    /**
     * Delete.
     *
     * @param cpf the cpf
     */
    public void delete(String cpf) {
        log.info("delete: {}", cpf);
        deleteUserUseCase.execute(cpf);
    }

    /**
     * Update.
     *
     * @param request the request
     * @param cpf     the cpf
     */
    public void update(UpdateUserRequest request, String cpf) {
        log.info("update: cpf {} - request {}", cpf, request);
        updateUserUseCase.execute(UpdateUserRequestMapper.INSTANCE.requestToEntityToUpdate(request), cpf);
    }

    /**
     * Find by cpf read user response.
     *
     * @param cpf the cpf
     * @return the read user response
     */
    public ReadUserResponse findByCpf(String cpf) {
        log.info("findByCpf: {}", cpf);

        return ReadUserResponseMapper.INSTANCE.entityToResponse(findUserByCpfUseCase.execute(cpf));
    }

    /**
     * Find by name list.
     *
     * @param name the name
     * @return the list
     */
    public List<ReadUserResponse> findByName(String name) {
        log.info("findByName: {}", name);

        return ReadUserResponseMapper.INSTANCE.entitiesToResponse(findUsersByNameUseCase.execute(name));
    }

    /**
     * Find by surname list.
     *
     * @param surname the surname
     * @return the list
     */
    public List<ReadUserResponse> findBySurname(String surname) {
        log.info("findBySurname: {}", surname);

        return ReadUserResponseMapper.INSTANCE.entitiesToResponse(findUsersBySurnameUseCase.execute(surname));
    }
}
