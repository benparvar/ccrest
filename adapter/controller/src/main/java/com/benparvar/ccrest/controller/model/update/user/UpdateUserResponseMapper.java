package com.benparvar.ccrest.controller.model.update.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * The interface Update user response mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UpdateUserResponseMapper {
    /**
     * The constant INSTANCE.
     */
    UpdateUserResponseMapper INSTANCE = Mappers.getMapper(UpdateUserResponseMapper.class);

    /**
     * Entity to response update user response.
     *
     * @param entity the entity
     * @return the update user response
     */
    UpdateUserResponse entityToResponse(UserEntity entity);

    /**
     * Response to entity user entity.
     *
     * @param response the response
     * @return the user entity
     */
    UserEntity responseToEntity(UpdateUserResponse response);
}
