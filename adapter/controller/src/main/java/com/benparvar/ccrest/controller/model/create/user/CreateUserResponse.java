package com.benparvar.ccrest.controller.model.create.user;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * The type Create user response.
 */
public class CreateUserResponse implements Serializable {
    private static final long serialVersionUID = 1418935843392268163L;
    private String cpf;
    private String name;
    private String surname;
    private String birthDate;
    private String address;
    private List<String> phones;
    private List<String> emails;
    private Boolean active;

    /**
     * Gets cpf.
     *
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Sets cpf.
     *
     * @param cpf the cpf
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phones.
     *
     * @return the phones
     */
    public List<String> getPhones() {
        return phones;
    }

    /**
     * Sets phones.
     *
     * @param phones the phones
     */
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    /**
     * Gets emails.
     *
     * @return the emails
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * Sets emails.
     *
     * @param emails the emails
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param active the active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateUserResponse response = (CreateUserResponse) o;
        return Objects.equals(cpf, response.cpf) &&
                Objects.equals(name, response.name) &&
                Objects.equals(surname, response.surname) &&
                Objects.equals(birthDate, response.birthDate) &&
                Objects.equals(address, response.address) &&
                Objects.equals(phones, response.phones) &&
                Objects.equals(emails, response.emails) &&
                Objects.equals(active, response.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cpf, name, surname, birthDate, address, phones, emails, active);
    }

    @Override
    public String toString() {
        return "CreateUserResponse{" +
                "cpf='" + cpf + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", address='" + address + '\'' +
                ", phones=" + phones +
                ", emails=" + emails +
                ", active=" + active +
                '}';
    }
}
