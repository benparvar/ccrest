package com.benparvar.ccrest.controller.model.read.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface Read user response mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ReadUserResponseMapper {

    /**
     * The constant INSTANCE.
     */
    ReadUserResponseMapper INSTANCE = Mappers.getMapper(ReadUserResponseMapper.class);

    /**
     * Entity to response update user response.
     *
     * @param entity the entity
     * @return the update user response
     */
    ReadUserResponse entityToResponse(UserEntity entity);

    /**
     * Response to entity user entity.
     *
     * @param response the response
     * @return the user entity
     */
    UserEntity responseToEntity(ReadUserResponse response);


    /**
     * Entities to response list.
     *
     * @param entities the entities
     * @return the list
     */
    List<ReadUserResponse> entitiesToResponse(List<UserEntity> entities);
}
