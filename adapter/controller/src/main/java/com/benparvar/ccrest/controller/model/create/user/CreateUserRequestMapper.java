package com.benparvar.ccrest.controller.model.create.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface Create user request mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CreateUserRequestMapper {
    /**
     * The constant INSTANCE.
     */
    CreateUserRequestMapper INSTANCE = Mappers.getMapper(CreateUserRequestMapper.class);

    /**
     * Entity to request create user request.
     *
     * @param entity the entity
     * @return the create user request
     */
    CreateUserRequest entityToRequest(UserEntity entity);

    /**
     * Request to entity user entity.
     *
     * @param request the request
     * @return the user entity
     */
    UserEntity requestToEntity(CreateUserRequest request);

    /**
     * Request to entities list.
     *
     * @param request the request
     * @return the list
     */
    List<UserEntity> requestToEntities(List<CreateUserRequest> request);
}
