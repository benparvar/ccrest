package com.benparvar.ccrest.controller.model.update.user;

import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.domain.entity.UserEntityToUpdate;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * The interface Update user request mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UpdateUserRequestMapper {
    /**
     * The constant INSTANCE.
     */
    UpdateUserRequestMapper INSTANCE = Mappers.getMapper(UpdateUserRequestMapper.class);

    /**
     * Entity to request update user request.
     *
     * @param entity the entity
     * @return the update user request
     */
    UpdateUserRequest entityToRequest(UserEntity entity);

    /**
     * Request to entity user entity.
     *
     * @param request the request
     * @return the user entity
     */
    UserEntity requestToEntity(UpdateUserRequest request);

    /**
     * Entity to update to request update user request.
     *
     * @param entity the entity
     * @return the update user request
     */
    UpdateUserRequest entityToUpdateToRequest(UserEntityToUpdate entity);

    /**
     * Request to entity to update user entity to update.
     *
     * @param request the request
     * @return the user entity to update
     */
    UserEntityToUpdate requestToEntityToUpdate(UpdateUserRequest request);
}