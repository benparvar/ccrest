package com.benparvar.ccrest.gateway;

import com.benparvar.ccrest.database.h2db.UserDatabase;
import com.benparvar.ccrest.database.h2db.model.UserModel;
import com.benparvar.ccrest.database.h2db.model.UserModelMapper;
import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.exception.UserAlreadyExistsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * The type User gateway test.
 */
public class UserGatewayTest {
    private final UserDatabase userDataBase = mock(UserDatabase.class);
    private UserGateway gateway;

    private final Long VALID_ID = 1L;
    private final String VALID_CPF = "324.880.350-92";
    private final String ANOTHER_VALID_CPF = "039.370.270-76";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final LocalDate VALID_ENTITY_LOCAL_DATE = LocalDate.of(2019, 12, 31);
    private final LocalDate VALID_LOCAL_DATE = LocalDate.now().minusYears(20L);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        gateway = new UserGateway(userDataBase);
    }

    /**
     * Save with null entity will fail.
     */
    @Test
    public void saveWithNullEntityWillFail() {

        assertThrows(IllegalArgumentException.class, () -> {
            UserEntity entity = null;
            try {
                gateway.saveUser(entity);
            } catch (IllegalArgumentException e) {
                verify(userDataBase, times(0)).save(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }


    /**
     * Save and existent valid entity will fail.
     */
    @Test
    public void saveAndExistentValidEntityWillFail() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();

        UserModel model = (UserModelMapper.INSTANCE
                .entityToModel(entity));

        when(userDataBase.findByCpf(VALID_CPF)).thenReturn(model);

        assertThrows(UserAlreadyExistsException.class, () -> {
            try {
                UserEntity savedEntity = gateway.saveUser(entity);
            } catch (UserAlreadyExistsException e) {
                verify(userDataBase, times(1)).findByCpf(VALID_CPF);
                verify(userDataBase, times(0)).saveAndFlush(any());
                throw e;
            }
            fail("should throw an UserAlreadyExistException");
        });
    }

    /**
     * Save with valid entity will success.
     */
    @Test
    public void saveWithValidEntityWillSuccess() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_ENTITY_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();

        UserModel model = (UserModelMapper.INSTANCE
                .entityToModel(entity));

        UserModel savedModel = (UserModelMapper.INSTANCE
                .entityToModel(entity));
        savedModel.setId(VALID_ID);

        when(userDataBase.saveAndFlush(model)).thenReturn(savedModel);

        UserEntity savedEntity = gateway.saveUser(entity);

        assertNotNull(savedEntity);
        assertTrue(savedEntity.getCpf().equals(entity.getCpf()), "CPF must be equal");
        assertTrue(savedEntity.getName().equals(entity.getName()), "Name must be equal");
        assertTrue(savedEntity.getSurname().equals(entity.getSurname()), "Surname must be equal");
        assertTrue(savedEntity.getBirthDate().equals(entity.getBirthDate()), "Birth date must be equal");
        assertTrue(savedEntity.getAddress().equals(entity.getAddress()), "Address must be equal");
        assertTrue(savedEntity.getPhones().equals(entity.getPhones()), "Phones must be equal");
        assertTrue(savedEntity.getEmails().equals(entity.getEmails()), "Emails must be equal");
        assertTrue(savedEntity.equals(entity));

        verify(userDataBase, times(1)).findByCpf(VALID_CPF);
        verify(userDataBase, times(1)).saveAndFlush(any());
    }

    /**
     * Save with null entity list will fail.
     */
    @Test
    public void saveWithNullEntityListWillFail() {
        when(userDataBase.saveAll(any())).thenThrow(new IllegalArgumentException("Some error"));

        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = null;
            try {
                gateway.saveUser(entities);
            } catch (IllegalArgumentException e) {
                assertEquals("Some error", e.getMessage());
                verify(userDataBase, times(1)).saveAll(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Find by cpf list with null list will fail.
     */
    @Test
    public void findByCpfListWithNullListWillFail() {
        when(userDataBase.findByCpfIn(any())).thenThrow(new IllegalArgumentException("Some error"));

        assertThrows(IllegalArgumentException.class, () -> {
            List<String> cpfList = null;
            try {
                gateway.findByCpfIn(cpfList);
            } catch (IllegalArgumentException e) {
                assertEquals("Some error", e.getMessage());
                verify(userDataBase, times(1)).findByCpfIn(cpfList);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Find by cpf list with empty list will return empty.
     */
    @Test
    public void findByCpfListWithEmptyListWillReturnEmpty() {
        when(userDataBase.findByCpfIn(any())).thenReturn(Collections.emptyList());

        List<String> cpfList = Collections.emptyList();

        List<UserEntity> result = gateway.findByCpfIn(cpfList);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        verify(userDataBase, times(1)).findByCpfIn(cpfList);
    }

    /**
     * Find by cpf list with a valid and existent cpf list will return with success.
     */
    @Test
    public void findByCpfListWithAValidAndExistentCpfListWillReturnWithSuccess() {
        UserModel userModel1 = new UserModel();
        userModel1.setCpf(VALID_CPF);

        UserModel userModel2 = new UserModel();
        userModel2.setCpf(ANOTHER_VALID_CPF);

        List<UserModel> userModelList = Arrays.asList(userModel1, userModel2);

        when(userDataBase.findByCpfIn(Arrays.asList(VALID_CPF, ANOTHER_VALID_CPF))).thenReturn(userModelList);

        List<String> cpfList = Arrays.asList(VALID_CPF, ANOTHER_VALID_CPF);

        List<UserEntity> result = gateway.findByCpfIn(cpfList);
        assertNotNull(result);
        assertEquals(2, result.size());

        verify(userDataBase, times(1)).findByCpfIn(cpfList);
    }

    /**
     * Reactivate with null list will fail.
     */
    @Test
    public void reactivateWithNullListWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            List<UserEntity> entities = null;
            try {
                gateway.reactivate(entities);
            } catch (IllegalArgumentException e) {
                assertEquals("User entity list cannot be null", e.getMessage());
                verify(userDataBase, never()).reactivate(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Reactivate with empty list will return with success.
     */
    @Test
    public void reactivateWithEmptyListWillReturnWithSuccess() {
        List<UserEntity> entities = Collections.EMPTY_LIST;

        int reactivated = gateway.reactivate(entities);
        Assertions.assertEquals(0, reactivated);
        verify(userDataBase, times(1)).reactivate(any());
    }

    /**
     * Reactivate with list o non existent cpf will return with success.
     */
    @Test
    public void reactivateWithListONonExistentCpfWillReturnWithSuccess() {
        UserEntity userEntity1 = UserEntity.newBuilder().cpf(VALID_CPF).active(Boolean.FALSE).build();
        UserEntity userEntity2 = UserEntity.newBuilder().cpf(ANOTHER_VALID_CPF).active(Boolean.FALSE).build();

        List<UserEntity> entities = Arrays.asList(userEntity1, userEntity2);
        when(userDataBase.reactivate(Arrays.asList(VALID_CPF, ANOTHER_VALID_CPF))).thenReturn(0);

        int reactivated = gateway.reactivate(entities);
        Assertions.assertEquals(0, reactivated);
        verify(userDataBase, times(1)).reactivate(any());
    }

    /**
     * Reactivate with list with an existent cpf will return with success.
     */
    @Test
    public void reactivateWithListWithAnExistentCpfWillReturnWithSuccess() {
        UserEntity userEntity1 = UserEntity.newBuilder().cpf(VALID_CPF).active(Boolean.FALSE).build();
        UserEntity userEntity2 = UserEntity.newBuilder().cpf(ANOTHER_VALID_CPF).active(Boolean.TRUE).build();

        List<UserEntity> entities = Arrays.asList(userEntity1, userEntity2);
        when(userDataBase.reactivate(Arrays.asList(VALID_CPF, ANOTHER_VALID_CPF))).thenReturn(1);

        int reactivated = gateway.reactivate(entities);
        Assertions.assertEquals(1, reactivated);
        verify(userDataBase, times(1)).reactivate(any());
    }

    /**
     * Delete with null cpf will fail.
     */
    @Test
    public void deleteWithNullCPFWillFail() {
        when(userDataBase.deleteUsingCpf(any())).thenThrow(new IllegalArgumentException("Some error"));

        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = null;
            try {
                gateway.deleteUsingCpf(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                verify(userDataBase, times(0)).deleteUsingCpf(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Delete with empty cpf will fail.
     */
    @Test
    public void deleteWithEmptyCPFWillFail() {
        when(userDataBase.deleteUsingCpf(any())).thenThrow(new IllegalArgumentException("Some error"));

        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = "";
            try {
                gateway.deleteUsingCpf(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("CPF cannot be null or empty", e.getMessage());
                verify(userDataBase, times(0)).deleteUsingCpf(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Delete with valid cpf will success.
     */
    @Test
    public void deleteWithValidCPFWillSuccess() {
        when(userDataBase.deleteUsingCpf(VALID_CPF)).thenReturn(1);
        String cpf = VALID_CPF;

        int result = gateway.deleteUsingCpf(cpf);

        assertEquals(1, result);
        verify(userDataBase, times(1)).deleteUsingCpf(VALID_CPF);
    }

    /**
     * Update with a non existent cpf will success and return zero.
     */
    @Test
    public void updateWithANonExistentCpfWillSuccessAndReturnZero() {
        when(userDataBase.findByCpf(VALID_CPF)).thenReturn(null);

        int result = gateway.updateUser(UserEntity.newBuilder().cpf(VALID_CPF).build());

        assertEquals(0, result);
        verify(userDataBase, times(1)).findByCpf(VALID_CPF);
        verify(userDataBase, times(0)).save(any());
    }

    /**
     * Update with a existent cpf will success and return one.
     */
    @Test
    public void updateWithAExistentCpfWillSuccessAndReturnOne() {
        UserModel model = new UserModel();
        model.setCpf(VALID_CPF);

        when(userDataBase.findByCpf(VALID_CPF)).thenReturn(model);

        int result = gateway.updateUser(UserEntity.newBuilder().cpf(VALID_CPF).build());

        assertEquals(1, result);
        verify(userDataBase, times(1)).findByCpf(VALID_CPF);
        verify(userDataBase, times(1)).save(model);
    }

    /**
     * Find by cpf with null cpf will fail.
     */
    @Test
    public void findByCpfWithNullCPFWillFail() {
        when(userDataBase.findByCpf(any())).thenThrow(new IllegalArgumentException("Some error"));

        assertThrows(IllegalArgumentException.class, () -> {
            String cpf = null;
            try {
                gateway.findByCpf(cpf);
            } catch (IllegalArgumentException e) {
                assertEquals("Some error", e.getMessage());
                verify(userDataBase, times(1)).findByCpf(any());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Find by cpf with empty cpf will fail.
     */
    @Test
    public void findByCpfWithEmptyCPFWillFail() {
        String cpf = "";

        when(userDataBase.findByCpf(any())).thenReturn(null);

        UserEntity result = gateway.findByCpf(cpf);

        assertNull(result);
        verify(userDataBase, times(1)).findByCpf(any());
    }

    /**
     * Find by cpf with valid cpf but non existent will success.
     */
    @Test
    public void findByCpfWithValidCPFButNonExistentWillSuccess() {
        String cpf = VALID_CPF;
        UserModel userModel = null;

        when(userDataBase.findByCpf(VALID_CPF)).thenReturn(userModel);

        UserEntity result = gateway.findByCpf(cpf);

        assertNull(result);
        verify(userDataBase, times(1)).findByCpf(VALID_CPF);
    }

    /**
     * Find by cpf with valid cpf will success.
     */
    @Test
    public void findByCpfWithValidCPFWillSuccess() {
        String cpf = VALID_CPF;
        UserModel userModel = new UserModel();
        userModel.setCpf(VALID_CPF);

        when(userDataBase.findByCpf(VALID_CPF)).thenReturn(userModel);

        UserEntity result = gateway.findByCpf(cpf);

        assertNotNull(result);
        verify(userDataBase, times(1)).findByCpf(VALID_CPF);
    }

    /**
     * Find by name with a non existent will success.
     */
    @Test
    public void findByNameWithANonExistentWillSuccess() {
        String name = VALID_NAME;

        when(userDataBase.findByName(name)).thenReturn(Collections.EMPTY_LIST);

        List<UserEntity> result = gateway.findByName(name);

        assertNotNull(result);
        assertTrue(result.isEmpty());
        verify(userDataBase, times(1)).findByName(name);
    }

    /**
     * Find by name with a existent will success.
     */
    @Test
    public void findByNameWithAExistentWillSuccess() {
        String name = VALID_NAME;
        UserModel userModel = new UserModel();
        userModel.setName(name);

        when(userDataBase.findByName(name)).thenReturn(Arrays.asList(userModel));

        List<UserEntity> result = gateway.findByName(name);

        assertNotNull(result);
        assertEquals(1, result.size());
        verify(userDataBase, times(1)).findByName(name);
    }

    /**
     * Find by surname with a non existent will success.
     */
    @Test
    public void findBySurnameWithANonExistentWillSuccess() {
        String surname = VALID_SURNAME;

        when(userDataBase.findBySurname(surname)).thenReturn(Collections.EMPTY_LIST);

        List<UserEntity> result = gateway.findBySurname(surname);

        assertNotNull(result);
        assertTrue(result.isEmpty());
        verify(userDataBase, times(1)).findBySurname(surname);
    }

    /**
     * Find by surname with a existent will success.
     */
    @Test
    public void findBySurnameWithAExistentWillSuccess() {
        String surname = VALID_SURNAME;
        UserModel userModel = new UserModel();
        userModel.setSurname(surname);

        when(userDataBase.findBySurname(surname)).thenReturn(Arrays.asList(userModel));

        List<UserEntity> result = gateway.findBySurname(surname);

        assertNotNull(result);
        assertEquals(1, result.size());
        verify(userDataBase, times(1)).findBySurname(surname);
    }
}
