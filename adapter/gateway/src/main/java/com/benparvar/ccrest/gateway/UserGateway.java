package com.benparvar.ccrest.gateway;

import com.benparvar.ccrest.database.h2db.UserDatabase;
import com.benparvar.ccrest.database.h2db.model.UserModel;
import com.benparvar.ccrest.database.h2db.model.UserModelMapper;
import com.benparvar.ccrest.domain.entity.UserEntity;
import com.benparvar.ccrest.usecase.create.user.SaveUser;
import com.benparvar.ccrest.usecase.create.users.FindUsersByCpfList;
import com.benparvar.ccrest.usecase.create.users.ReactivateUsers;
import com.benparvar.ccrest.usecase.create.users.SaveUsers;
import com.benparvar.ccrest.usecase.delete.DeleteUser;
import com.benparvar.ccrest.usecase.exception.UserAlreadyExistsException;
import com.benparvar.ccrest.usecase.read.user.cpf.FindUserByCpf;
import com.benparvar.ccrest.usecase.read.users.name.FindUsersByName;
import com.benparvar.ccrest.usecase.read.users.surname.FindUsersBySurname;
import com.benparvar.ccrest.usecase.update.UpdateUser;
import com.benparvar.ccrest.usecase.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Relational user gateway.
 */
public class UserGateway implements SaveUser, SaveUsers, FindUsersByCpfList, ReactivateUsers, DeleteUser, UpdateUser,
        FindUserByCpf, FindUsersByName, FindUsersBySurname {
    private final Logger log = LoggerFactory.getLogger(UserGateway.class);
    private final UserDatabase userDataBase;
    private static final String USER_LIST_CANNOT_BE_NULL = "User entity list cannot be null";
    private static final String USER_CANNOT_BE_NULL = "User entity cannot be null";
    private static final String CPF_CANNOT_BE_EMPTY = "CPF cannot be null or empty";

    /**
     * Instantiates a new Relational user gateway.
     *
     * @param userDataBase the user data base
     */
    public UserGateway(UserDatabase userDataBase) {
        this.userDataBase = userDataBase;
    }

    @Override
    public UserEntity saveUser(UserEntity userEntity) {
        log.info("saveUser: {}", userEntity);
        Assert.notNull(userEntity, USER_CANNOT_BE_NULL);

        UserEntity foundedEntity = findByCpf(userEntity.getCpf());
        if (null != foundedEntity)
            throw new UserAlreadyExistsException();

        UserModel userModel = userDataBase.saveAndFlush(UserModelMapper.INSTANCE
                .entityToModel(userEntity));
        log.info("saveUser saved: {}", userModel);

        return UserModelMapper.INSTANCE.modelToEntity(userModel);
    }

    @Override
    public List<UserEntity> findByCpfIn(List<String> cpfList) {
        log.info("findByCpfIn: {}", cpfList);
        List<UserModel> userModels = userDataBase.findByCpfIn(cpfList);
        log.info("encountered in database: {}", userModels);

        return UserModelMapper.INSTANCE.modelToEntities(userModels);
    }

    @Override
    public int reactivate(List<UserEntity> userEntities) {
        log.info("reactivate: {}", userEntities);
        Assert.notNull(userEntities, USER_LIST_CANNOT_BE_NULL);

        return userDataBase.reactivate(userEntities.stream()
                .map(UserEntity::getCpf)
                .collect(Collectors.toList()));
    }

    @Override
    public List<UserEntity> saveUser(List<UserEntity> userEntities) {
        log.info("saveUser: {}", userEntities);
        List<UserModel> userModels = userDataBase.saveAll(UserModelMapper.INSTANCE
                .entitiesToModel(userEntities));
        userDataBase.flush();
        log.info("saveUser database saved: {}", userModels);

        return UserModelMapper.INSTANCE.modelToEntities(userModels);
    }

    @Override
    public int deleteUsingCpf(String cpf) {
        log.info("deleteUsingCpf: {}", cpf);
        Assert.hasText(cpf, CPF_CANNOT_BE_EMPTY);

        return userDataBase.deleteUsingCpf(cpf);
    }

    @Override
    public UserEntity findByCpf(String cpf) {
        log.info("findByCpf: {}", cpf);
        UserModel userModel = userDataBase.findByCpf(cpf);
        log.info("encountered in database: {}", userModel);

        return UserModelMapper.INSTANCE.modelToEntity(userModel);
    }

    @Override
    public List<UserEntity> findByName(String name) {
        log.info("findByName: {}", name);
        List<UserModel> userModels = userDataBase.findByName(name);
        log.info("encountered in database: {}", userModels);

        return UserModelMapper.INSTANCE.modelToEntities(userModels);
    }

    @Override
    public List<UserEntity> findBySurname(String surname) {
        log.info("findBySurname: {}", surname);
        List<UserModel> userModels = userDataBase.findBySurname(surname);
        log.info("encountered in database: {}", userModels);

        return UserModelMapper.INSTANCE.modelToEntities(userModels);
    }

    @Override
    public int updateUser(UserEntity userEntity) {
        log.info("updateUser: {}", userEntity);
        UserModel userModel = userDataBase.findByCpf(userEntity.getCpf());
        if (null != userModel) {
            userModel.setName(userEntity.getName());
            userModel.setSurname(userEntity.getSurname());
            userModel.setCpf(userEntity.getCpf());
            userModel.setActive(userEntity.getActive());
            userModel.setAddress(userEntity.getAddress());
            userModel.setEmails(userEntity.getEmails());
            userModel.setPhones(userEntity.getPhones());
            userModel.setBirthDate(userEntity.getBirthDate());
            userDataBase.save(userModel);
            userDataBase.flush();

            return 1;
        }

        return 0;
    }
}
