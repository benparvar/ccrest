module com.benparvar.ccrest.gateway {
    exports com.benparvar.ccrest.gateway;
    requires org.slf4j;
    requires com.benparvar.ccrest.domain;
    requires com.benparvar.ccrest.database;
    requires com.benparvar.ccrest.usecase;
    requires spring.data.jpa;
}