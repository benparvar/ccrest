module com.benparvar.ccrest.database {
    exports com.benparvar.ccrest.database.h2db;
    exports com.benparvar.ccrest.database.h2db.model;

    requires com.benparvar.ccrest.domain;
    requires com.benparvar.ccrest.usecase;
    requires com.h2database;
    requires org.slf4j;
    requires org.mapstruct;
    requires java.sql;
    requires java.persistence;
    requires java.validation;
    requires spring.data.jpa;
    requires spring.data.commons;
    requires spring.tx;
    requires spring.context;
    requires org.hibernate.validator;
}
