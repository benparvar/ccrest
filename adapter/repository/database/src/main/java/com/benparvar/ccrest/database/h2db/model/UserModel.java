package com.benparvar.ccrest.database.h2db.model;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * The type User model.
 */
@Entity
public class UserModel implements Serializable {
    private static final long serialVersionUID = 3515131615082231877L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "cpf", unique = true)
    @CPF(message = "CPF must be valid")
    private String cpf;
    @NotEmpty(message = "Name cannot be empty")
    private String name;
    @NotEmpty(message = "Surname cannot be empty")
    private String surname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Birth date cannot be empty")
    private LocalDate birthDate;
    @NotEmpty(message = "Address cannot be empty")
    private String address;
    @ElementCollection
    @NotEmpty(message = "phones cannot be empty")
    private List<String> phones;
    @ElementCollection
    @NotEmpty(message = "Emails cannot be empty")
    private List<String> emails;
    @NotNull
    private Boolean active;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets cpf.
     *
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Sets cpf.
     *
     * @param cpf the cpf
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phones.
     *
     * @return the phones
     */
    public List<String> getPhones() {
        return phones;
    }

    /**
     * Sets phones.
     *
     * @param phones the phones
     */
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    /**
     * Gets emails.
     *
     * @return the emails
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * Sets emails.
     *
     * @param emails the emails
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param active the active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserModel userModel = (UserModel) o;
        return cpf.equals(userModel.cpf) &&
                name.equals(userModel.name) &&
                surname.equals(userModel.surname) &&
                birthDate.equals(userModel.birthDate) &&
                address.equals(userModel.address) &&
                phones.equals(userModel.phones) &&
                emails.equals(userModel.emails) &&
                active.equals(userModel.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cpf, name, surname, birthDate, address, phones, emails, active);
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", cpf='" + cpf + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", phones=" + phones +
                ", emails=" + emails +
                ", active=" + active +
                '}';
    }
}