package com.benparvar.ccrest.database.h2db;

import com.benparvar.ccrest.database.h2db.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The interface User database.
 */
public interface UserDatabase extends JpaRepository<UserModel, String> {
    /**
     * Find by cpf in list.
     *
     * @param cpfList the cpf list
     * @return the list
     */
    List<UserModel> findByCpfIn(List<String> cpfList);

    /**
     * Find by cpf user model.
     *
     * @param cpf the cpf
     * @return the user model
     */
    UserModel findByCpf(String cpf);

    /**
     * Reactivate int.
     *
     * @param cpfList the cpf list
     * @return the int
     */
    @Transactional
    @Modifying
    @Query(value = "update UserModel u set u.active = true where u.cpf in :cpfList and u.active = false")
    int reactivate(@Param("cpfList") List<String> cpfList);

    /**
     * Delete using cpf int.
     *
     * @param cpf the cpf
     * @return the int
     */
    @Transactional
    @Modifying
    @Query(value = "update UserModel u set u.active = false where u.cpf = :cpf")
    int deleteUsingCpf(@Param("cpf") String cpf);

    /**
     * Find by name list.
     *
     * @param name the name
     * @return the list
     */
    List<UserModel> findByName(String name);

    /**
     * Find by surname list.
     *
     * @param surname the surname
     * @return the list
     */
    List<UserModel> findBySurname(String surname);

}
