package com.benparvar.ccrest.database.h2db.model;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface User model mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserModelMapper {
    /**
     * The constant INSTANCE.
     */
    UserModelMapper INSTANCE = Mappers.getMapper(UserModelMapper.class);

    /**
     * Entity to model user model.
     *
     * @param entity the entity
     * @return the user model
     */
    UserModel entityToModel(UserEntity entity);

    /**
     * Model to entity user entity.
     *
     * @param model the model
     * @return the user entity
     */
    UserEntity modelToEntity(UserModel model);

    /**
     * Entities to model list.
     *
     * @param entity the entity
     * @return the list
     */
    List<UserModel> entitiesToModel(List<UserEntity> entity);

    /**
     * Model to entities list.
     *
     * @param model the model
     * @return the list
     */
    List<UserEntity> modelToEntities(List<UserModel> model);
}
