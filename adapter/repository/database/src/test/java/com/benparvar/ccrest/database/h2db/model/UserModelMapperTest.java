package com.benparvar.ccrest.database.h2db.model;

import com.benparvar.ccrest.domain.entity.UserEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type User model mapper test.
 */
public class UserModelMapperTest {
    private final String VALID_CPF = "324.880.350-92";
    private final String VALID_NAME = "Name";
    private final String VALID_SURNAME = "Surname";
    private final LocalDate VALID_LOCAL_DATE = LocalDate.now().minusYears(20L);
    private final String VALID_ADDRESS = "Acacia Avenue, 666";
    private final List<String> VALID_PHONES = Arrays.asList("(11) 94378-3192");
    private final List<String> VALID_EMAILS = Arrays.asList("name@email.com");
    private final Boolean VALID_ACTIVE_TRUE = Boolean.TRUE;

    /**
     * Should map empty entity to model.
     */
    @Test
    public void shouldMapEmptyEntityToModel() {
        UserEntity entity = UserEntity.newBuilder().build();
        UserModel model = UserModelMapper.INSTANCE.entityToModel(entity);

        assertNotNull(model);
    }

    /**
     * Should map entity to model.
     */
    @Test
    public void shouldMapEntityToModel() {
        UserEntity entity = UserEntity.newBuilder()
                .cpf(VALID_CPF)
                .name(VALID_NAME)
                .surname(VALID_SURNAME)
                .birthDate(VALID_LOCAL_DATE)
                .address(VALID_ADDRESS)
                .phones(VALID_PHONES)
                .emails(VALID_EMAILS)
                .active(VALID_ACTIVE_TRUE)
                .build();
        UserModel model = UserModelMapper.INSTANCE.entityToModel(entity);

        assertNotNull(model);
        assertTrue(entity.getCpf().equals(model.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(model.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(model.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().equals(model.getBirthDate()), "Birth date must be equal");
        assertTrue(entity.getAddress().equals(model.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(model.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(model.getEmails()), "Emails must be equal");
        assertTrue(entity.getActive().equals(model.getActive()), "Active must be equal");
    }

    /**
     * Should map empty model to entity.
     */
    @Test
    public void shouldMapEmptyModelToEntity() {
        UserModel model = new UserModel();
        UserEntity entity = UserModelMapper.INSTANCE.modelToEntity(model);

        assertNotNull(entity);
    }

    /**
     * Should map a null list of entities to model.
     */
    @Test
    public void shouldMapANullListOfEntitiesToModel() {
        List<UserEntity> entities = null;
        List<UserModel> model = UserModelMapper.INSTANCE.entitiesToModel(entities);

        assertNull(model);
    }

    /**
     * Should map a empty list of entities to model.
     */
    @Test
    public void shouldMapAEmptyListOfEntitiesToModel() {
        List<UserEntity> entities = Collections.emptyList();
        List<UserModel> model = UserModelMapper.INSTANCE.entitiesToModel(entities);

        assertNotNull(model);
        assertTrue(model.isEmpty(), "UserModel list must be empty");
    }

    @Test
    public void shouldMapANullListOfModelToEntities() {
        List<UserModel> models = null;
        List<UserEntity> entities =
                UserModelMapper.INSTANCE.modelToEntities(models);

        assertNull(entities);
    }

    @Test
    public void shouldMapAEmptyListOfModelToEntities() {
        List<UserModel> models = Collections.EMPTY_LIST;
        List<UserEntity> entities =
                UserModelMapper.INSTANCE.modelToEntities(models);

        assertNotNull(entities);
        assertTrue(entities.isEmpty());
    }

    @Test
    public void shouldMapAListOfModelToEntities() {
        UserModel model = new UserModel();
        model.setCpf(VALID_CPF);
        model.setName(VALID_NAME);
        model.setSurname(VALID_SURNAME);
        model.setBirthDate(VALID_LOCAL_DATE);
        model.setAddress(VALID_ADDRESS);
        model.setPhones(VALID_PHONES);
        model.setEmails(VALID_EMAILS);
        model.setActive(VALID_ACTIVE_TRUE);

        List<UserEntity> entities =
                UserModelMapper.INSTANCE.modelToEntities(Arrays.asList(model));

        assertNotNull(entities);
        assertFalse(entities.isEmpty());

        UserEntity entity = entities.get(0);

        assertTrue(entity.getCpf().equals(model.getCpf()), "CPF must be equal");
        assertTrue(entity.getName().equals(model.getName()), "Name must be equal");
        assertTrue(entity.getSurname().equals(model.getSurname()), "Surname must be equal");
        assertTrue(entity.getBirthDate().equals(model.getBirthDate()), "Birth date must be equal");
        assertTrue(entity.getAddress().equals(model.getAddress()), "Address must be equal");
        assertTrue(entity.getPhones().equals(model.getPhones()), "Phones must be equal");
        assertTrue(entity.getEmails().equals(model.getEmails()), "Emails must be equal");
        assertTrue(entity.getActive().equals(model.getActive()), "Active must be equal");
    }
}