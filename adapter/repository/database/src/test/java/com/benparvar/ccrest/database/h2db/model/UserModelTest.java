package com.benparvar.ccrest.database.h2db.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type User model test.
 */
class UserModelTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UserModel obj1 = new UserModel();
        obj1.setId(1L);
        obj1.setCpf("336.519.850-40");
        obj1.setName("Hashy");
        obj1.setSurname("Ilusion");
        obj1.setBirthDate(LocalDate.of(2017, 1, 2));
        obj1.setAddress("Desulional St, 21");
        obj1.setEmails(Arrays.asList("del@ded.gov"));
        obj1.setPhones(Arrays.asList("(11) 8888-1231"));

        UserModel obj2 = new UserModel();
        obj2.setId(1L);
        obj2.setCpf("336.519.850-40");
        obj2.setName("Hashy");
        obj2.setSurname("Ilusion");
        obj2.setBirthDate(LocalDate.of(2017, 1, 2));
        obj2.setAddress("Desulional St, 21");
        obj2.setEmails(Arrays.asList("del@ded.gov"));
        obj2.setPhones(Arrays.asList("(11) 8888-1231"));

        assertEquals(obj1.hashCode(), obj2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UserModel obj1 = new UserModel();
        obj1.setId(1L);
        obj1.setCpf("336.519.850-40");
        obj1.setName("Hashy");
        obj1.setSurname("Ilusion");
        obj1.setBirthDate(LocalDate.of(2017, 1, 2));
        obj1.setAddress("Desulional St, 21");
        obj1.setEmails(Arrays.asList("del@ded.gov"));
        obj1.setPhones(Arrays.asList("(11) 8888-1231"));
        obj1.setActive(Boolean.FALSE);

        UserModel obj2 = new UserModel();
        obj2.setId(1L);
        obj2.setCpf("336.519.850-40");
        obj2.setName("Hashy");
        obj2.setSurname("Ilusion");
        obj2.setBirthDate(LocalDate.of(2017, 1, 2));
        obj2.setAddress("Desulional St, 21");
        obj2.setEmails(Arrays.asList("del@ded.gov"));
        obj2.setPhones(Arrays.asList("(11) 8888-1231"));
        obj2.setActive(Boolean.FALSE);

        assertEquals(obj1, obj2);
    }
}